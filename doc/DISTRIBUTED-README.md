
## Hello, World !

You've downloaded _Nanogodo_. **It's a friendly nano-robot.**
It's also an eponymous game. A free one ; even better, a _libre_ one !


## Report bugs

It's over here : https://framagit.org/godotrio/nanogodo/issues


## Source code

You can peruse the source code and assets of this game here :
https://framagit.org/godotrio/nanogodo

_Nanogodo is always glad to have friends patching its kernel,
shaving its bootloader, and oiling its subroutines._


## Authors

Who cares? We're all in this together!


## Thanks

- The Godot team & community
- The Godot team & community _(again)_
- HVRI, 'cause he's awesome, even if he thinks this is a big mistake
- Meterpreter, for the ruthless bug hunting
- Grilvhor, for his questions and peer review
- Willou, for his eager beta-testing
- Myriam, estie d'bestie !
- Françoise & Xavier, for their unfaltering support
- Georges, \o/
- The Factorio team
- The Godot team & community _(we mean it!)_
- The toxic sods who get to _bite my shiny nano ass!_


## License

Do what you want, and don't complain.
