extends Label

func _ready():
	var scale = Vector2(0.42,0.42)
	var half = get_size() / 2.0
	set_position(get_position() - half)
	set_pivot_offset(half)
	set_rotation(TAU / 4.0)
	set_scale(scale)
	set_theme(preload("res://data/base/graphics/deco/cryptic_theme.tres"))
	set_self_modulate(Color(0.42,0.42,0.42,0.42))

func set_text(text):
	var reversed = ""
	for i in range(text.length()-1, -1, -1):
		if not text[i] in [" "]:
			reversed += text[i]
	.set_text(reversed)