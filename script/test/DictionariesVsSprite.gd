extends Node

const Utils = preload("res://script/lib/Utils.gd")
var test_spawner = {}
# Basic benchmark template

# Benchmarking Dictionaries linux x64 i7 4790k
# TIMER REPOPULATE (str vs vec)
#  total: 4222 ms
#  lap 1 : 2886 ms (216%)
#  lap 2 : 1336 ms (100%)
# TIMER CREATE KEYS (str vs vec)
#  total: 1851 ms
#  lap 1 : 1775 ms (2336%)           <= Vector2 wins !
#  lap 2 : 76 ms (100%)
# TIMER ACCESS WITH HAS (str vs vec)
#  total: 351 ms
#  lap 1 : 171 ms (100%)
#  lap 2 : 180 ms (105%)
class CheapSpriteList:
	var list = Array()
	
	func add(cs):
		list.append(cs)
	func size():
		return list.size()
	func get_position(id):
		return list[id].position
		
	func free():
		for i in range(size()):
			list[i].free()
		.free()

class CheapSprite:
	var position
	
	func _init(position):
		self.position = position
#Benchmarking Dictionaries Win7 x64 Phenom II 1090T
#TIMER REPOPULATE (str vs vec)
#  total: 8259 ms
#  lap 1 : 5710 ms (224%) Memory
#  lap 2 : 2549 ms (100%) Memory
#TIMER CREATE KEYS (str vs vec)
#  total: 3102 ms
#  lap 1 : 2918 ms (1586%) Memory
#  lap 2 : 184 ms (100%) Memory
#TIMER ACCESS WITH HAS (str vs vec)
#  total: 823 ms
#  lap 1 : 393 ms (100%) Memory
#  lap 2 : 430 ms (109%) Memory
var idx =0
var csl
var tex = preload("res://script/test/erythrocyte_for_background.png")
func _ready():
	print("Integers")
	
	var sprites_count = 20000
	

	csl = CheapSpriteList.new()
	# DUMMY PICKUPS
	for i in range(sprites_count):
		test_spawner = { i: Sprite.new() }
		test_spawner[i].position = Vector2(rand_range(0,1200), rand_range(0,800))
		test_spawner[i].texture = tex
		test_spawner[i].set_global_rotation(rand_range(0,TAU))
		get_node("Node2D").add_child(test_spawner[i])
#	for i in range(sprites_count):
#		test_spawner[i] = Vector2(rand_range(0,800), rand_range(0,400))
#		test_spawner[i].position = Vector2(rand_range(0,800), rand_range(0,400))
		
#		.add_child(test_spawner[i])

#	for i in range(sprites_count):
#		csl.add(CheapSprite.new(Vector2(rand_range(0,800), rand_range(0,400))))
#		test_spawner[i].position = Vector2(rand_range(0,800), rand_range(0,400))
		
#		.add_child(test_spawner[i])
	
	# FIXTURES FROM TOKENS
#	for i in range(csl.size()):
		
#		draw_texture(tex, test_spawner[i])
#		if get_viewport().get_visible_rect().has_point(csl.get_position(i)):
#			var a = csl.get_position(i)
#			VisualServer.canvas_item_add_texture_rect( get_canvas_item(), Rect2(a, Vector2(16,16)), tex, false)
	prints("1 << 62 =", 1 << 62)
	prints("(1 << 62) + 1 =", (1 << 62) + 1)
	prints("1 << 63 =", 1 << 63)              # Lowest int
	prints("(1 << 63) - 1 =", (1 << 63) - 1)  # Biggest int
	prints("1 << 64 =", 1 << 64)
	prints("1 << 65 =", 1 << 65)


func _draw():
	idx +=1
	if idx <=1:
		for i in range(csl.size()):
			
	#		draw_texture(tex, test_spawner[i])
			if get_viewport().get_visible_rect().has_point(csl.get_position(i)):
				var a = csl.get_position(i)
				VisualServer.canvas_item_add_texture_rect( get_canvas_item(), Rect2(a, Vector2(16,16)), tex, false)
	else: 
		csl.free()
#	print("Dictionaries Serialization")
#	var test_dict = {
#		Vector2(42,-13): "My thing",
#		Vector2(0,0): "My other thing",
#	}
#	var desr_dict = str2var(var2str(test_dict))
#	print(test_dict)
#	print(desr_dict)
#	assert desr_dict[Vector2(42,-13)] == "My thing"
#	#assert test_dict == desr_dict  # false T_T
#
#	print("Dictionaries Concaternation")
#
#	var dict_a = {'a': 1, 'b': 2}
#	var dict_b = {'c': 5, 'b': 4}
#	var dict_c = Utils.merge(dict_a, dict_b)
#	assert dict_c.b == 4
#	print(dict_c)
#
#	var dict_d = {'a': 1, 'b': {'c': 2, 'd': 3}}
#	var dict_e = {'a': 5, 'b': {'c': 6, 'e': 4}}
#	var dict_f = Utils.merge(dict_d, dict_e)
#	print(dict_f)
#	assert dict_f.b.c == 6
#	assert dict_f.b.e == 4
#	assert dict_f.b.d == 3
#	assert dict_f.a == 5
#
#	print("Benchmarking Dictionaries")
#
#	start_timer("REPOPULATE (str vs vec)")
#	repopulate_dict_str()
#	register_lap()
#	repopulate_dict_vec()
#	register_lap()
#	close_timer()
#
#	start_timer("CREATE KEYS (str vs vec)")
#	create_keys_str()
#	register_lap()
#	create_keys_vec()
#	register_lap()
#	close_timer()
#
#	start_timer("ACCESS WITH HAS (str vs vec)")
#	test_has_str()
#	register_lap()
#	test_has_vec()
#	register_lap()
#	close_timer()
#
#	# Peace-of-mind assertions, about key equality with `==` and not `===`.
#	assert dict_str["(42, 42)"] == 1
#	assert not dict_str.has("(41, 43)")
#	assert dict_vec[Vector2(42,42)] == 1
#	assert not dict_vec.has(Vector2(41,43))

################################################################################

var iterations = 100000
var dict_str = {}
var dict_vec = {}

func create_keys_str():
	for i in range(iterations):
		"(%d, %d)" % [i, i]

func create_keys_vec():
	for i in range(iterations):
		Vector2(i, i)

func repopulate_dict_str():
	dict_str = {}
	for i in range(iterations):
		var k = "(%d, %d)" % [i, i]
		if not dict_str.has(k):
			dict_str[k] = 1
#		for j in range(100):
#			dict_str.has(k)

func repopulate_dict_vec():
	dict_vec = {}
	for i in range(iterations):
		var k = Vector2(i, i)
		if not dict_vec.has(k):
			dict_vec[k] = 1
#		for j in range(100):
#			dict_vec.has(k)

func test_has_str():
	for i in range(iterations):
		dict_str.has("(7, 7)")
		dict_str.has("(1, 0)")

func test_has_vec():
	for i in range(iterations):
		dict_vec.has(Vector2(7, 7))
		dict_vec.has(Vector2(1, 0))


### TIMER ######################################################################

var timer_name
var timer_start
var timer_laps
func start_timer(name="SOMETHING"):
	timer_name = name
	timer_laps = []
	timer_start = {
		'time':OS.get_ticks_msec(),
		'usage':OS.get_static_memory_usage()
	}
func register_lap():
	timer_laps.append({
		'time': OS.get_ticks_msec(),
		'usage': OS.get_static_memory_usage()
	})
func close_timer():
	var total = OS.get_ticks_msec() - timer_start.time
	assert total > 0
	var laps = []
	var best_lap_time = total
	print("TIMER %s" % timer_name)
	print("  total: %d ms" % total)
	var prev_lap = timer_start
	for i in range(timer_laps.size()):
		var lap_time = timer_laps[i].time - prev_lap.time
		assert lap_time >= 0
		laps.append({
			'time': lap_time,
			'usage': timer_laps[i].usage - prev_lap.usage
		})
		if lap_time < best_lap_time:
			best_lap_time = lap_time
		prev_lap = timer_laps[i]
	if best_lap_time == 0:
		best_lap_time = 1
		printerr("Your best lap time took less than 1ms. Too fast!")
	for i in range(laps.size()):
		print("  lap %d : %d ms (%d%%) Memory Usage: %d" % [
			i+1, laps[i].time, round(laps[i].time / (0.01 * best_lap_time)),
			laps[i].usage
		])
