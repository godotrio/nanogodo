extends Node

# Global Object Danger = GOD

# This is a singleton, but also an injected dependency into all of our game
# bricks (pickups, fixtures, …). A bit redundant, but since we're going for
# global, we might as well go all the way. Advice would be appreciated.

# It's responsibilities are :

# A. General Configuration
# This is where the bricks and campaigns configurations are stored.
# Bricks configuration may later be loaded dynamically,
# to be able to be altered by Levels.

# B. Sugar Methods
# We also add sugar methods here, to get layers, spawn or despawn entities,
# or show snacks, for example.

# C. Bootstrap other singletons (such as Blueprint)
# Singletons that need an instance (that can't be all static), and that do not
# belong in Game, we're putting them here. Still looking for a better design.

# Singletons : http://docs.godotengine.org/en/stable/learning/step_by_step/singletons_autoload.html
# Is `Globals.set()` still working in 3.x ? Hmmm…

################################################################################

# Angels
const Blueprint = preload("res://script/core/Blueprint.gd")
#const Math = preload("res://script/core/Math.gd")

# Interfaces
#const Inserter = preload("res://script/brick/fixture/Inserter.gd")
const Pickup = preload("res://script/core/Pickup.gd")

# Utils
const Utils = preload("res://script/lib/Utils.gd")


### GENERAL CONFIGURATION ######################################################

var gitlab_url = "https://framagit.org/godotrio/nanogodo/wikis/home"


### FIXTURES ###################################################################

# We probably want to add the texture here as well?
var fixtures = {
	'wall': {
		'slug':     'wall',
		'layer':    'General',
		'script':   preload("res://script/brick/fixture/Wall.gd"),
	},
	'chest': {
		'slug':     'chest',
		'layer':    'General',
		'script':   preload("res://script/brick/fixture/Chest.gd"),
	},
	'belt': {
		'slug':     'belt',
		'layer':    'Belts',
		'script':   preload("res://script/brick/fixture/Belt.gd"),
	},
	'inserter': {
		'slug':     'inserter',
		'layer':    'General',
		'script':   preload("res://script/brick/fixture/Inserter.gd"),
	},
	'inserter_long': {
		'slug':     'inserter_long',
		'layer':    'General',
		'script':   preload("res://script/brick/fixture/InserterLong.gd"),
	},
	'factory': {
		'slug':     'factory',
		'layer':    'General',
		'script':   preload("res://script/brick/fixture/Factory.gd"),
	},
	'pickup_spawner': {
		'slug':     'pickup_spawner',
		'layer':    'General',
		'script':   preload("res://script/brick/fixture/PickupSpawner.gd"),
	},
	'pickup_despawner': {
		'slug':     'pickup_despawner',
		'layer':    'General',
		'script':   preload("res://script/brick/fixture/PickupDespawner.gd"),
	},
}


### PICKUPS ####################################################################

var pickups = {
	# TRALALABOUDIN ############################################################
	'pouloupinium': {
		'slug':     'pouloupinium',
		'name':     'Pouloupinium',
		'stack':    1,
		'script':   preload("res://script/brick/pickup/Pouloupinium.gd"),
		'texture':  preload("res://data/base/graphics/pickup/pouloupinium/pouloupinium.png"),
	},
	# UNUSED ###################################################################
	'cholesterol': {
		'slug':     'cholesterol',
		'name':     'Cholesterol',
		'stack':    1,
		'script':   preload("res://script/brick/pickup/Cholesterol.gd"),
		'texture':  preload("res://data/base/graphics/pickup/cholesterol/cholesterol.png"),
	},
	# BASE #####################################################################
	'calcium': {
		'slug':     'calcium',
		'name':     'Calcium',
		'stack':    99,
		'script':   preload("res://script/brick/pickup/Calcium.gd"),
		'texture':  preload("res://data/base/graphics/pickup/calcium/calcium.png"),
	},
	'carbon': {
		'slug':     'carbon',
		'name':     'Carbon',
		'stack':    99,
		'script':   preload("res://script/brick/pickup/Carbon.gd"),
		'texture':  preload("res://data/base/graphics/pickup/carbon/carbon.png"),
	},
	'chlorine': {
		'slug':     'chlorine',
		'name':     'Chlorine',
		'stack':    99,
		'script':   preload("res://script/brick/pickup/Chlorine.gd"),
		'texture':  preload("res://data/base/graphics/pickup/chlorine/chlorine.png"),
	},
	'copper': {
		'slug':     'copper',
		'name':     'Copper',
		'stack':    99,
		'script':   preload("res://script/brick/pickup/Copper.gd"),
		'texture':  preload("res://data/base/graphics/pickup/copper/copper.png"),
	},
	'hydrogen': {
		'slug':     'hydrogen',
		'name':     'Hydrogen',
		'stack':    99,
		'script':   preload("res://script/brick/pickup/Hydrogen.gd"),
		'texture':  preload("res://data/base/graphics/pickup/hydrogen/hydrogen.png"),
	},
	'iodine': {
		'slug':     'iodine',
		'name':     'Iodine',
		'stack':    99,
		'script':   preload("res://script/brick/pickup/Iodine.gd"),
		'texture':  preload("res://data/base/graphics/pickup/iodine/iodine.png"),
	},
	'iron': {
		'slug':     'iron',
		'name':     'Iron',
		'stack':    99,
		'script':   preload("res://script/brick/pickup/Iron.gd"),
		'texture':  preload("res://data/base/graphics/pickup/iron/iron.png"),
	},
	'manganese': {
		'slug':     'manganese',
		'name':     'Manganese',
		'stack':    99,
		'script':   preload("res://script/brick/pickup/Manganese.gd"),
		'texture':  preload("res://data/base/graphics/pickup/manganese/manganese.png"),
	},
	'magnesium': {
		'slug':     'magnesium',
		'name':     'Magnesium',
		'stack':    99,
		'script':   preload("res://script/brick/pickup/Magnesium.gd"),
		'texture':  preload("res://data/base/graphics/pickup/magnesium/magnesium.png"),
	},
	'nitrogen': {
		'slug':     'nitrogen',
		'name':     'Nitrogen',
		'stack':    99,
		'script':   preload("res://script/brick/pickup/Nitrogen.gd"),
		'texture':  preload("res://data/base/graphics/pickup/nitrogen/nitrogen.png"),
	},
	'oxygen': {
		'slug':     'oxygen',
		'name':     'Oxygen',
		'stack':    99,
		'script':   preload("res://script/brick/pickup/Oxygen.gd"),
		'texture':  preload("res://data/base/graphics/pickup/oxygen/oxygen.png"),
	},
	'phosphorus': {
		'slug':     'phosphorus',
		'name':     'Phosphorus',
		'stack':    99,
		'script':   preload("res://script/brick/pickup/Phosphorus.gd"),
		'texture':  preload("res://data/base/graphics/pickup/phosphorus/phosphorus.png"),
	},
	'potassium': {
		'slug':     'potassium',
		'name':     'Potassium',
		'stack':    99,
		'script':   preload("res://script/brick/pickup/Potassium.gd"),
		'texture':  preload("res://data/base/graphics/pickup/potassium/potassium.png"),
	},
	'sodium': {
		'slug':     'sodium',
		'name':     'Sodium',
		'stack':    99,
		'script':   preload("res://script/brick/pickup/Sodium.gd"),
		'texture':  preload("res://data/base/graphics/pickup/sodium/sodium.png"),
	},
	'sulfur': {
		'slug':     'sulfur',
		'name':     'Sulfur',
		'stack':    99,
		'script':   preload("res://script/brick/pickup/Sulfur.gd"),
		'texture':  preload("res://data/base/graphics/pickup/sulfur/sulfur.png"),
	},
	
	# BLOOD COMPONENTS #########################################################
	'neutrophil': { # white blood cells
		'slug':     'neutrophil',
		'name':     'Neutrophil',
		'stack':    50,
		'script':   preload("res://script/brick/pickup/Neutrophil.gd"),
		'texture':  preload("res://data/base/graphics/pickup/neutrophil/neutrophil.png"),
	},
	'lymphocyte': {
		'slug':     'lymphocyte',
		'name':     'Lymphocyte',
		'stack':    50,
		'script':   preload("res://script/brick/pickup/Lymphocyte.gd"),
		'texture':  preload("res://data/base/graphics/pickup/lymphocyte/lymphocyte.png"),
	},
	'monocyte': {
		'slug':     'monocyte',
		'name':     'Monocyte',
		'stack':    50,
		'script':   preload("res://script/brick/pickup/Monocyte.gd"),
		'texture':  preload("res://data/base/graphics/pickup/monocyte/monocyte.png"),
	},
	'eosinophil': {
		'slug':     'eosinophil',
		'name':     'Eosinophil',
		'stack':    50,
		'script':   preload("res://script/brick/pickup/Eosinophil.gd"),
		'texture':  preload("res://data/base/graphics/pickup/eosinophil/eosinophil.png"),
	},
	'basophil': {
		'slug':     'basophil',
		'name':     'Basophil',
		'stack':    50,
		'script':   preload("res://script/brick/pickup/Basophil.gd"),
		'texture':  preload("res://data/base/graphics/pickup/basophil/basophil.png"),
	},
	'platelets': {
		'slug':     'platelets',
		'name':     'Platelets',
		'stack':    50,
		'script':   preload("res://script/brick/pickup/Platelets.gd"),
		'texture':  preload("res://data/base/graphics/pickup/platelets/platelets.png"),
	},
	
	# INTERMEDIARY #############################################################
	'diamond': {
		'slug':     'diamond',  # https://youtu.be/oh0lrDg04yA
		'name':     'Diamond',
		'stack':    99,
		'script':   preload("res://script/brick/pickup/Diamond.gd"),
		'texture':  preload("res://data/base/graphics/pickup/diamond/diamond.png"),
	},
	'erythrocyte': {
		'slug':     'erythrocyte',
		'name':     'Erythrocyte',
		'stack':    10,
		'script':   preload("res://script/brick/pickup/Erythrocyte.gd"),
		'texture':  preload("res://data/base/graphics/pickup/erythrocyte/erythrocyte.png"),
	},
	'erythrocyte_o2': {
		'slug':     'erythrocyte_o2',
		'name':     'Oxygenated erythrocyte',
		'stack':    10,
		'script':   preload("res://script/brick/pickup/ErythrocyteO2.gd"),
		'texture':  preload("res://data/base/graphics/pickup/erythrocyte_o2/erythrocyte_o2.png"),
	},
	'dioxygen': {
		'slug':     'dioxygen',
		'name':     'Dioxygen',
		'stack':    10,
		'script':   preload("res://script/brick/pickup/Dioxygen.gd"),
		'texture':  preload("res://data/base/graphics/pickup/dioxygen/dioxygen.png"),
	},
	'carbon_dioxide': {
		'slug':     'carbon_dioxide',
		'name':     'Carbon dioxide',
		'stack':    10,
		'script':   preload("res://script/brick/pickup/CarbonDioxide.gd"),
		'texture':  preload("res://data/base/graphics/pickup/carbon_dioxide/carbon-dioxide.png"),
	},
	'water': {
		'slug':     'water',
		'name':     'Water',
		'stack':    99,
		'script':   preload("res://script/brick/pickup/Water.gd"),
		'texture':  preload("res://data/base/graphics/pickup/water/water.png"),
	},
	'water_droplet': {
		'slug':     'water_droplet',
		'name':     'Water Droplet',
		'stack':    99,
		'script':   preload("res://script/brick/pickup/WaterDroplet.gd"),
		'texture':  preload("res://data/base/graphics/pickup/water/water-droplet.png"),
	},
	'blood_droplet': {
		'slug':     'blood',
		'name':     'Blood droplet',
		'stack':    99,
		'script':   preload("res://script/brick/pickup/BloodDroplet.gd"),
		'texture':  preload("res://data/base/graphics/pickup/blood/blood.png"),
	},
	# EASTER PICKUPS ###########################################################
	'egg': {
		'slug':     'egg',
		'name':     'Egg',
		'stack':    99,
		'script':   preload("res://script/brick/pickup/WhiteEgg.gd"),
		'texture':  preload("res://data/base/graphics/pickup/eggs/white-egg.png"),
	},
	'chocolate_egg': {
		'slug':     'chocolate_egg',
		'name':     'Chocolate Egg',
		'stack':    99,
		'script':   preload("res://script/brick/pickup/ChocolateEgg.gd"),
		'texture':  preload("res://data/base/graphics/pickup/eggs/chocolate-egg.png"),
	},
	'pink_egg': {
		'slug':     'pink_egg',
		'name':     'Pink Egg',
		'stack':    99,
		'script':   preload("res://script/brick/pickup/PinkEgg.gd"),
		'texture':  preload("res://data/base/graphics/pickup/eggs/pink-egg.png"),
	},
	'blue_egg': {
		'slug':     'blue_egg',
		'name':     'Blue Egg',
		'stack':    99,
		'script':   preload("res://script/brick/pickup/BlueEgg.gd"),
		'texture':  preload("res://data/base/graphics/pickup/eggs/blue-egg.png"),
	},
	'green_egg': {
		'slug':     'green_egg',
		'name':     'Green Egg',
		'stack':    99,
		'script':   preload("res://script/brick/pickup/GreenEgg.gd"),
		'texture':  preload("res://data/base/graphics/pickup/eggs/green-egg.png"),
	},
	'chocolate': {
		'slug':     'chocolate',
		'name':     'Chocolate',
		'stack':    99,
		'script':   preload("res://script/brick/pickup/Chocolate.gd"),
		'texture':  preload("res://data/base/graphics/pickup/chocolate/chocolate.png"),
	},
	'pink_potion': {
		'slug':     'pink_potion',
		'name':     'Pink Potion',
		'stack':    99,
		'script':   preload("res://script/brick/pickup/PinkPotion.gd"),
		'texture':  preload("res://data/base/graphics/pickup/potions/pink.png"),
	},
	'green_potion': {
		'slug':     'green_potion',
		'name':     'Green Potion',
		'stack':    99,
		'script':   preload("res://script/brick/pickup/GreenPotion.gd"),
		'texture':  preload("res://data/base/graphics/pickup/potions/green.png"),
	},
	'blue_potion': {
		'slug':     'blue_potion',
		'name':     'Blue Potion',
		'stack':    99,
		'script':   preload("res://script/brick/pickup/BluePotion.gd"),
		'texture':  preload("res://data/base/graphics/pickup/potions/blue.png"),
	},

	
	
	# PICKUPS OF FIXTURES ######################################################
	'wall': {
		'slug':     'wall',
		'name':     'Wall',
		'stack':    99,
		'script':   preload("res://script/brick/pickup/Wall.gd"),
		'texture':  preload("res://data/base/graphics/pickup/wall/wall.png"),
		'fixture':  'wall'
	},
	'chest': {
		'slug':     'chest',
		'name':     'Chest',
		'stack':    99,
		'script':   preload("res://script/brick/pickup/Chest.gd"),
		'texture':  preload("res://data/base/graphics/pickup/chest/chest.png"),
		'fixture':  'chest'
	},
	'belt': {
		'slug':     'belt',
		'name':     'Conveyor belt',
		'stack':    99,
		'script':   preload("res://script/brick/pickup/Belt.gd"),
		'texture':  preload("res://data/base/graphics/pickup/belt/belt.png"),
		'fixture':  'belt'
	},
	'inserter': {
		'slug':     'inserter',
		'name':     'Inserter',
		'stack':    99,
		'script':   preload("res://script/brick/pickup/Inserter.gd"),
		'texture':  preload("res://data/base/graphics/pickup/inserter/inserter.png"),
		'fixture':  'inserter'
	},
	'inserter_long': {
		'slug':     'inserter_long',
		'name':     'Long Inserter',
		'stack':    99,
		'script':   preload("res://script/brick/pickup/InserterLong.gd"),
		'texture':  preload("res://data/base/graphics/pickup/inserter_long/inserter-long.png"),
		'fixture':  'inserter_long'
	},
	'factory': {
		'slug':     'factory',
		'name':     'Factory',
		'stack':    99,
		'script':   preload("res://script/brick/pickup/Factory.gd"),
		'texture':  preload("res://data/base/graphics/pickup/factory/factory.png"),
		'fixture':  'factory'
	},
	'pickup_spawner': {
		'slug':     'pickup_spawner',
		'name':     'Pickup spawner',
		'stack':    99,
		'script':   preload("res://script/brick/pickup/PickupSpawner.gd"),
		'texture':  preload("res://data/base/graphics/pickup/pickup_spawner/pickup_spawner.png"),
		'fixture':  'pickup_spawner'
	},
}


### RECIPES ####################################################################

var recipes = {
	'diamond': {
		'slug': 'diamond',
		'time': 2001, # ms
		'texture': preload("res://data/base/graphics/pickup/diamond/diamond.png"),
		'intakes': [
			{
				'pickup_type': 'carbon',
				'quantity': 4
			},
		],
		'outakes': [
			{
				'pickup_type': 'diamond',
				'quantity': 1
			},
		],
	},
	'chest': {
		'slug': 'chest',
		'time': 3003, # ms
		'texture': preload("res://data/base/graphics/pickup/chest/chest.png"),
		'intakes': [
			{
				'pickup_type': 'carbon',
				'quantity': 2
			},
			{
				'pickup_type': 'iron',
				'quantity': 6
			},
			{
				'pickup_type': 'diamond',
				'quantity': 1
			},
		],
		'outakes': [
			{
				'pickup_type': 'chest',
				'quantity': 1
			},
		],
	},
	'belt': {
		'slug': 'belt',
		'time': 3003, # ms
		'texture': preload("res://data/base/graphics/pickup/belt/belt.png"),
		'intakes': [
			{
				'pickup_type': 'erythrocyte',
				'quantity': 1
			},
			{
				'pickup_type': 'diamond',
				'quantity': 1
			},
		],
		'outakes': [
			{
				'pickup_type': 'belt',
				'quantity': 1
			},
		],
	},
	'inserter': {
		'slug': 'inserter',
		'time': 3003, # ms
		'texture': preload("res://data/base/graphics/pickup/inserter/inserter.png"),
		'intakes': [
			{
				'pickup_type': 'hydrogen',
				'quantity': 1
			},
			{
				'pickup_type': 'oxygen',
				'quantity': 1
			},
			{
				'pickup_type': 'nitrogen',
				'quantity': 1
			},
		],
		'outakes': [
			{
				'pickup_type': 'inserter',
				'quantity': 1
			},
		],
	},
	'inserter_long': {
		'slug': 'inserter_long',
		'time': 6006, # ms
		'texture': preload("res://data/base/graphics/pickup/inserter_long/inserter-long.png"),
		'intakes': [
			{
				'pickup_type': 'inserter',
				'quantity': 1
			},
				{
				'pickup_type': 'hydrogen',
				'quantity': 2
			},
			{
				'pickup_type': 'carbon',
				'quantity': 4
			},
		],
		'outakes': [
			{
				'pickup_type': 'inserter_long',
				'quantity': 1
			},
		],
	},
	'factory': {
		'slug': 'factory',
		'time': 42000, # ms
		'texture': preload("res://data/base/graphics/pickup/factory/factory.png"),
		'intakes': [
			{
				'pickup_type': 'diamond',
				'quantity': 12
			},
		],
		'outakes': [
			{
				'pickup_type': 'factory',
				'quantity': 1
			},
		],
	},
	'erythrocyte_o2': {
		'slug': 'erythrocyte_o2',
		'time': 60000, # ms
		'texture': preload("res://data/base/graphics/pickup/erythrocyte_o2/erythrocyte_o2.png"),
		'intakes': [
			{
				'pickup_type': 'erythrocyte',
				'quantity': 1
			},
			{
				'pickup_type': 'oxygen',
				'quantity': 2
			},
		],
		'outakes': [
			{
				'pickup_type': 'erythrocyte_o2',
				'quantity': 1
			},
		],
	},
	'water': {
		'slug': 'water',
		'time': 3003, # ms
		'texture': preload("res://data/base/graphics/pickup/water/water.png"),
		'intakes': [
			{
				'pickup_type': 'hydrogen',
				'quantity': 2
			},
			{
				'pickup_type': 'oxygen',
				'quantity': 1
			},
		],
		'outakes': [
			{
				'pickup_type': 'water',
				'quantity': 1
			},
		],
	},
	'dioxygen': {
		'slug': 'dioxygen',
		'time': 5000, # ms
		'texture': preload("res://data/base/graphics/pickup/dioxygen/dioxygen.png"),
		'intakes': [
			{
				'pickup_type': 'oxygen',
				'quantity': 2
			},
		],
		'outakes': [
			{
				'pickup_type': 'dioxygen',
				'quantity': 1
			},
		],
	},
	'carbon_dioxide': {
		'slug': 'carbon_dioxide',
		'time': 5000, # ms
		'texture': preload("res://data/base/graphics/pickup/carbon_dioxide/carbon-dioxide.png"),
		'intakes': [
			{
				'pickup_type': 'oxygen',
				'quantity': 2
			},
				{
				'pickup_type': 'carbon',
				'quantity': 1
			},
		],
		'outakes': [
			{
				'pickup_type': 'carbon_dioxide',
				'quantity': 1
			},
		],
	},
	'water_droplet': {
		'slug': 'water_droplet',
		'time': 20000, # ms
		'texture': preload("res://data/base/graphics/pickup/water/water-droplet.png"),
		'intakes': [
			{
				'pickup_type': 'water',
				'quantity': 20
			},
		],
		'outakes': [
			{
				'pickup_type': 'water_droplet',
				'quantity': 1
			},
		],
	},
	'blood_droplet': {
		'slug': 'blood_droplet',
		'time': 20000, # ms
		'texture': preload("res://data/base/graphics/pickup/blood/blood.png"),
		'intakes': [
			{
				'pickup_type': 'water',
				'quantity': 20
			},
			{
				'pickup_type': 'erythrocyte_o2',
				'quantity': 4
			},
		],
		'outakes': [
			{
				'pickup_type': 'blood_droplet',
				'quantity': 1
			},
		],
	},
	
	
	'dismantle_erythrocyte': {
		'slug': 'dismantle_erythrocyte',
		'time': 3000, # ms
		'texture': preload("res://data/base/graphics/pickup/erythrocyte/dismantle_erythrocyte.png"),
		'intakes': [
			{
				'pickup_type': 'erythrocyte',
				'quantity': 1
			},
		],
		'outakes': [
			{
				'pickup_type': 'iron',
				'quantity': 1
			},
			{
				'pickup_type': 'carbon',
				'quantity': 2
			},
		],
	},
	
	# EASTER RECIPES ##########################################################
	'chocolate': {
		'slug': 'chocolate',
		'time': 6006, # ms
		'texture': preload("res://data/base/graphics/pickup/chocolate/chocolate.png"),
		'intakes': [
			{
				'pickup_type': 'oxygen',
				'quantity': 2
			},
			{
				'pickup_type': 'hydrogen',
				'quantity': 7
			},
			{
				'pickup_type': 'nitrogen',
				'quantity': 4
			},
			{
				'pickup_type': 'carbon',
				'quantity': 2
			},
		],
		'outakes': [
			{
				'pickup_type': 'chocolate',
				'quantity': 1
			},
		],
	},
	'chocolate_egg': {
		'slug': 'chocolate_egg',
		'time': 40000, # ms
		'texture': preload("res://data/base/graphics/pickup/eggs/chocolate-egg.png"),
		'intakes': [
			{
				'pickup_type': 'egg',
				'quantity': 1
			},
			{
				'pickup_type': 'chocolate',
				'quantity': 1
			},
		],
		'outakes': [
			{
				'pickup_type': 'chocolate_egg',
				'quantity': 1
			},
		],
	},
	'green_egg': {
		'slug': 'green_egg',
		'time': 60000, # ms
		'texture': preload("res://data/base/graphics/pickup/eggs/green-egg.png"),
		'intakes': [
			{
				'pickup_type': 'chocolate',
				'quantity': 1
			},
			{
				'pickup_type': 'green_potion',
				'quantity': 1
			},
			{
				'pickup_type': 'egg',
				'quantity': 1
			},
		],
		'outakes': [
			{
				'pickup_type': 'green_egg',
				'quantity': 1
			},
		],
	},
	'blue_egg': {
		'slug': 'blue_egg',
		'time': 60000, # ms
		'texture': preload("res://data/base/graphics/pickup/eggs/blue-egg.png"),
		'intakes': [
			{
				'pickup_type': 'chocolate',
				'quantity': 1
			},
			{
				'pickup_type': 'blue_potion',
				'quantity': 1
			},
			{
				'pickup_type': 'egg',
				'quantity': 1
			},
		],
		'outakes': [
			{
				'pickup_type': 'blue_egg',
				'quantity': 1
			},
		],
	},
	'pink_egg': {
		'slug': 'pink_egg',
		'time': 60000, # ms
		'texture': preload("res://data/base/graphics/pickup/eggs/pink-egg.png"),
		'intakes': [
			{
				'pickup_type': 'chocolate',
				'quantity': 1
			},
			{
				'pickup_type': 'pink_potion',
				'quantity': 1
			},
			{
				'pickup_type': 'egg',
				'quantity': 1
			},
		],
		'outakes': [
			{
				'pickup_type': 'pink_egg',
				'quantity': 1
			},
		],
	},
}


### LEVELS #####################################################################

var campaigns = {
	'001_tutorial': {
		'slug': '001_tutorial',
		'levels': {
			'001_oxygenate': {
				'script': preload("res://script/level/TutorialLevel02.gd"),
#				'script': preload("res://script/level/HappyEasterLevel.gd"),
#				'script': preload("res://script/level/GameMakerLevel.gd"),
			}
		}
	}
}


### LAYERS #####################################################################

var layer_dict = Dictionary()

func add_to_layer(node, layer):
	if node.get_parent():
		node.get_parent().remove_child(node)
	layer.add_child(node)

func reset_layer():
	layer_dict.clear()

func register_layer(layer_name, layer_node):
	layer_dict[layer_name] = layer_node

func get_layer(layer_name):
	return layer_dict[layer_name]

func get_general_layer():
	return get_layer("General")

func get_feedback_layer():
	return get_layer("Feedback")

func get_pickups_layer():
	return get_layer("Pickups")

func get_belts_layer():
	return get_layer("Belts")

func get_earth_layer():
	return get_layer("Earth")

func get_fixed_hud_layer():
	return get_layer("FixedHud")

func get_hud_layer():
	return get_layer("Hud")

func get_interactive_hud_layer():
	return get_layer("InteractiveHud")

func get_cursor_layer():
	return get_layer("Cursor")

func add_to_general_layer(node):
	add_to_layer(node, get_general_layer())

func add_to_pickups_layer(node):
	add_to_layer(node, get_pickups_layer())


################################################################################

# Angels
var board
var game

#var _board = null
#var board setget set_board,get_board
#func set_board(b):
#	assert false # you can't do that !
#func get_board():
#	assert _board
#	return _board

var __blueprint = null
var blueprint setget set_blueprint,get_blueprint
func set_blueprint(b):
	breakpoint # you can't do that !
func get_blueprint():
	assert(__blueprint)
	return __blueprint


func _ready():
	# God is a singleton
	# God knws everythng
	__blueprint = Blueprint.new(self)
#	peep() # God is not watching
	print("⬡God⬢ is ready.") # hidden hexagons -_-

#func peep():
#	get_tree().connect("node_added",self,"_on_node_added_to_scene_tree")
#func _on_node_added_to_scene_tree(node):
#	pass


### SNACK ######################################################################

func snack(message):
	if self.game and self.game.character:
		self.game.character.spawn_floating_label(message)
	else:
		print("SNACK: %s" % message)


### PICKUP INVOCATIONS #########################################################

var spawned_pickups_count = 0
func increment_spawned_pickups_count():
	spawned_pickups_count = (spawned_pickups_count + 1) % (1<<30)

func spawn_pickup(pickup_type, stack=1):
	"""
	Creates a new pickup node. Does NOT add it to the scene tree.
	"""
	assert(self.pickups.has(pickup_type))
	var config = self.pickups[pickup_type]
	
	assert(stack >= 0)
	if stack == 0:
		stack = config.stack
	
	increment_spawned_pickups_count()
	
	var pickup = config.script.new()
	pickup.init(self, config, stack, spawned_pickups_count)
	return pickup

func despawn_pickup(pickup):
	if not pickup:
		return
	if not is_instance_valid(pickup):
		push_error("Trying to despawn an invalid pickup.")
		return
	if pickup.get_parent():
		pickup.get_parent().remove_child(pickup)
	pickup.queue_free()

#func destroy_pickup(pickup):
#	"""
#	Hmmmm. Deprecated? It's the board's business to forget its pickups, not god?
#	"""
#	if pickup.get_parent():
#		pickup.get_parent().remove_child(pickup)
#	get_board().forget_pickup(pickup)
#	pickup.queue_free()


### FIXTURE INVOCATIONS ########################################################

# Not sure which is best between "spawn" or "invoke".
# We're probably going to end up rolling with "spawn".

func spawn_fixture(fixture_type, tile, direction=null, init_args=Array()):
	if null == direction:
		direction = Vector2(1,0)
	var fixture_config = god.fixtures[fixture_type]
	var actual_init_args = [god, tile, direction]
	for arg in init_args:
		actual_init_args.append(arg)
	var fixture = fixture_config.script.new()
	fixture.callv('init', actual_init_args)
#	fixture.set_hex_direction(direction)
	fixture.position = self.board.hex_to_pix(tile)
	self.board.add_tile_fixture(tile, fixture)
	get_layer(fixture_config.layer).add_child(fixture)
	
	# Collect the pickups on the ground, on the tile
	var picks = self.board.get_all_pickups_on_grid()  # inefficient
	for p in picks:  # oh god
		if tile == self.board.pix_to_hex(p.position): # noooo
			self.board.remove_from_grid_if_on_it(p)
			if fixture.can_feed_pickup(p, p.position):
				fixture.feed_pickup(p, p.position)
			else:
				god.despawn_pickup(p)
	
	return fixture

func despawn_fixture(fixture):
	# Remove the fixture from the board
	self.board.remove_tile_fixture(fixture)
	# Remove the fixture from the scene
	if fixture.get_parent():
		fixture.get_parent().remove_child(fixture)
	# Close fixture interface if any is open
	# … todo
	# Let the fixtures override queue_free if necessary
	fixture.queue_free()

func invoke_from_token(token, tile):
	assert(token.substr(0, 3) == "<փ⋅")  # :(|)
	
	var dry_pickles = self.blueprint.decompress_string_pretty(token)
	if not dry_pickles:
		printerr("Failed to load token %s" % token)
		return
#	print(dry_pickles)
	var pickles = str2var(dry_pickles)
	if not pickles:
		printerr("Failed to parse pickles for %s :\n%s\n%s" % \
			[dry_pickles, dry_pickles, pickles.error_string])
		return
	
	# There's also pickles.meta <3
	for pickled_fixture in pickles.values:
		invoke_from_pickle(pickled_fixture, tile)

var invoked_pickles_count = 0
func invoke_from_pickle(rick, origin=null):  # deprecated alias
	spawn_fixture_from_pickle(rick, origin)

func spawn_fixture_from_pickle(rick, origin=null):
	invoked_pickles_count += 1
	assert(rick.has("fixture_type"))
	assert(rick.has("hex_position"))
	if null == origin:
		origin = Vector2(0,0)
	rick.hex_position = rick.hex_position + origin
	
	var fixture = self.fixtures[rick.fixture_type].script.from_pickle(self, rick)
	var fixture_name
	if rick.has("name"):
		fixture_name = rick.name
	else:
		fixture_name = "%s#%d" % [rick.fixture_type, invoked_pickles_count]
	fixture.name = fixture_name
	fixture.position = self.board.hex_to_pix(rick.hex_position)
	self.board.add_tile_fixture(rick.hex_position, fixture)
	if fixture is self.fixtures.belt.script:
		var picks = self.board.get_all_pickups_on_grid()  # inefficient
		for p in picks:
			if rick.hex_position == self.board.pix_to_hex(p.position):
				self.board.remove_from_grid_if_on_it(p)
				if fixture.can_feed_pickup(p, p.position):
					fixture.feed_pickup(p, p.position)
				else:
					god.despawn_pickup(p)
#		fixture.recompute_intake(true)
		# clean up : use layer info from config
		get_belts_layer().add_child(fixture)
	else:
		get_general_layer().add_child(fixture)


### GROUND INVOCATIONS #########################################################

func spawn_ground_from_pickle(ground_pickle):
	assert(ground_pickle.type == "hexagon") # others to implement?
	god.board.add_tile_ground(ground_pickle)
	god.game.add_ground_polygon_to_tile(
		ground_pickle.hex_position,
		ground_pickle.color
	)


### COW POWERS #################################################################

# deprecated ; use for tests only

var invoked_belts_count = 0
func invoke_belt(on_tile, to_next):
	"""
	Invoke a belt on the provided tile.
	Vector2 on_tile : a Hex(q,r) if we could
	Vector2 to_next : unitary vector to any adjacent tile, in hex space as well.
	"""
	invoked_belts_count += 1
	
	var to_prev = to_next * -1  # straight by default
	var conveyor = self.fixtures.belt.script.new(self, on_tile, to_next, to_prev)
	conveyor.name = "Belt#%d" % invoked_belts_count
	god.get_belts_layer().add_child(conveyor)
	############################################################################
	# WHY IN THE UNIVERSE IS *god* DEFINED HERE ? WTF !
	god.board.add_tile_fixture(on_tile, conveyor)
	# Falsified Hypotheses
	#  assert Game
	#  assert root
	# Ideas
	#  singleton? 
	#  preloading? <- 
	#  signals? <- … not really related at first glance
	############################################################################
	# We should maybe use get_board() as shown here.
	var picks = self.board.get_all_pickups_on_grid()  # inefficient
	############################################################################
	for p in picks:
		if on_tile == self.board.pix_to_hex(p.position):
			self.board.remove_from_grid_if_on_it(p)
			conveyor.feed_pickup(p, p.position, true)
	
	conveyor.recompute_intake(true)

var invoked_inserters_count = 0
func invoke_inserter(on_tile, to_outake):
	invoked_inserters_count += 1
	var inserter = self.fixtures.inserter.script.new()
	inserter.init(self, on_tile, to_outake)
	inserter.name = "TestInserter#%d" % invoked_inserters_count
	inserter.position = self.board.hex_to_pix(on_tile)
	self.board.add_tile_fixture(on_tile, inserter)
	get_general_layer().add_child(inserter)

var invoked_spawners_count = 0
func invoke_spawner(on_tile, to_outake, pickup_type):
	invoked_spawners_count += 1
	var spawner = self.fixtures.pickup_spawner.script.new()
	spawner.init(self, on_tile, to_outake, pickup_type)
	spawner.name = "Spawner#%d" % invoked_spawners_count
	spawner.position = self.board.hex_to_pix(on_tile)
	self.board.add_tile_fixture(on_tile, spawner)
	get_general_layer().add_child(spawner)


# NOT SURE WHERE TO STORE THIS #################################################

#static func str_to_vec2(s):  # deprecated
#	return Utils.str_to_vec2(s)

