#extends Angel

var god
func _init(kami_sama):
	self.god = kami_sama
	
	# sanity tests
	assert(EXCHANGE_TOKEN_CHARS.length() % 256 == 0)
	for i in range(EXCHANGE_TOKEN_CHARS.length()):
		if i != EXCHANGE_TOKEN_CHARS.find(EXCHANGE_TOKEN_CHARS[i]):
			prints("DUPLICATE", i, EXCHANGE_TOKEN_CHARS[i])
		assert(i == EXCHANGE_TOKEN_CHARS.find(EXCHANGE_TOKEN_CHARS[i]))


func serialize_fixtures_on(tiles, relative_to=null):
	return var2str(pickle_fixtures_on(tiles, relative_to))

func pickle_fixtures_on(tiles, relative_to=null):
	if not relative_to:
		relative_to = Vector2(0,0)
	
	var data = {
		"meta": {},  # use them later, maybe
		"values": []
	}
	
	for tile in tiles:
		if not god.board.has_tile_fixtures(tile):
			continue
		for fix in god.board.get_tile_fixtures(tile):
			data.values.append(fix.to_pickle(relative_to))
	
	return data


### STRING COMPRESSION #########################################################

# 1F 8B 08 <- because of GZIP
# <  փ  ⋅     let's make it awesome
const EXCHANGE_TOKEN_CHARS = \
	"Ξ" + \
	"#§Ӝ!&¡¤⋅Ֆµ¿" + \
	"϶աբգդեզըթժիլխծկձճմյ<չպջռվտրӡնքֆշ" + \
	"ΓΔΘΛ%ΠΣΥΦΧΨΩΫάέήΰαβδεζηθλμνξοπρςστυφχψωϋύϏϐϑϒϓϔϕϖϗϘϙϚϛϜϝϞϟϠϡϢϣϤϥϦϧϨϩϪϫϬϭϮϯϰϱϲϳϴϵ" + \
	"$123456789abcdeփfghijklmnopqrstuvwxyz" + \
	"ԲԳԴԵԶԷԸԹԺԻԼԽԾԿՀՁՂՃՄՅՆՇՈՉՊՋՌՍՎՏՐՑՒՓՔՕ" + \
	"@ABCDEFGHIJKLMNOPQRSTUVWXYZ" + \
	"ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞß"
	
static func compress_string(s):
	"""
	str s : utf8, usually `JSON.print(mydata)`.
	"""
	var bytes = s.to_utf8()
	var bytes_count = bytes.size()
	var compressed_bytes = bytes.compress(File.COMPRESSION_GZIP)
	# Let's append our byte count (maybe later, see note below)
	var compressed_str = ""
	for byte in compressed_bytes:
		compressed_str += "%02X" % byte

	return compressed_str

static func decompress_string(s):
	"""
	str s : utf8, usually an exchange token
	"""
	var compressed_bytes = []
	for i in range(0, s.length(), 2):
		var byte = ("0x"+s.substr(i, 2)).hex_to_int()  # oh god why
		compressed_bytes.append(byte)
	compressed_bytes = PoolByteArray(compressed_bytes)
	
	# 2097152 = 1<<21 (totally arbitrary length, 1<<31 won't work)
	# note: if the expected output size is too low, nothing is returned at all
	# workaround: store the length in the byte sequence and retrieve it here
	var bytes = compressed_bytes.decompress(2097152, File.COMPRESSION_GZIP)
	if not bytes.size():
		print("Could not decompress string of size %d : %s" % [s.size(), s])
	
	return bytes.get_string_from_utf8()


static func compress_string_pretty(s):
	"""
	str s : utf8, usually `JSON.print(mydata)` or `var2str(mydata)`.
	"""
	var bytes = s.to_utf8()
	var bytes_count = bytes.size()
	var compressed_bytes = bytes.compress(File.COMPRESSION_GZIP)
	# Let's append our byte count (later, see note below)
	var compressed_str = ""
	for byte in compressed_bytes:
		compressed_str += EXCHANGE_TOKEN_CHARS.substr(byte, 1)

	return compressed_str


static func decompress_string_pretty(s):
	"""
	str s : utf8, usually an exchange token
	"""
	var compressed_bytes = []
	for i in range(0, s.length()):
		var byte = EXCHANGE_TOKEN_CHARS.find(s[i])
		assert(byte > -1)  # ouch, if this happens
		compressed_bytes.append(byte)
	compressed_bytes = PoolByteArray(compressed_bytes)
	
	# 2097152 = 1<<21 (totally arbitrary length, 1<<31 won't work)
	# note: if the expected output size is too low, nothing is returned at all
	# workaround: store the length in the byte sequence and retrieve it here
	var bytes = compressed_bytes.decompress(2097152, File.COMPRESSION_GZIP)
	if not bytes.size():
		print("Could not decompress string of size %d : %s" % [s.length(), s])
	
	return bytes.get_string_from_utf8()

