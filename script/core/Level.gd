extends Node

# Base level for all levels.
# Mostly defines the interface all levels should have.

const Utils = preload("res://script/lib/Utils.gd")

var god
var game
func init(god, game):
	self.god = god
	self.game = game


### WORLD INTERFACE ############################################################

func create_world():
	"""
	Spawns:
		- fixtures
		- pickups
	Also sets game.ground_tiles
	"""
	assert(not "Override this method in your level definition.")


func load_from_data(data):
	var fixtures_pickles = data.level_data.fixtures
	for fixt_pickle in fixtures_pickles:
		god.spawn_fixture_from_pickle(fixt_pickle)
	var ground_pickles = data.level_data.ground
	for ground_pickle in ground_pickles:
		god.spawn_ground_from_pickle(ground_pickle)


### QUEST INTERFACE ############################################################

func is_complete():
	"""
	Returns Boolean
	"""
	assert(not "Override this method in your level definition.")


func get_mission_statement():
	"""
	The mission statement is displayed in the Quests GUI.
	"""
	return "Do whatever you want."


### PICKLING INTERFACE #########################################################

static func from_pickle(god, game, rick):
	"""
	Should create a new instance of the level and return it. (it's a factory!)
	God is the current game context. Not the best design, but convenient.
	Rick is a pickle, of course.
	You should override this.
	"""
	var level = .new()
	level.init(god, game)
	return level


func to_pickle():
	"""
	Should return serialization-ready, unpicklable data.
	- no Nodes, Resources, etc.
	- nothing but primitives (dicts of primitives are ok -- I'm pretty sure)
	- Vectors are okay too, since we're serializing with `var2str()` (not JSON)
	Extend this, and use `Utils.merge()` and `.to_pickle()`.
	"""
	return {}
