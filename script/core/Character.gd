extends KinematicBody2D

# The character is a companionable nano-robot named nanogodo.
# It is made of 3 animated sprites for head, body and tank treads.
# Here are the guidelines of the character :
# 1. Never enough bling bling
# 2. We don't care about perfs (it's The Character, and always on-screen!)
#    Also, we'll optimize later if need be. (GDNative FTW)

# This file is a huge mess.
# The character should not have to handle all the controls.
# Though it feels like a good idea to have all the controls in one place.
# But this place should probably not be the character.

################################################################################

const Hand = preload("res://script/core/Hand.gd")
const Inventory = preload("res://script/core/Inventory.gd")
const CumulativeThrottler = preload("res://script/lib/CumulativeThrottler.gd")

const Chest = preload("res://script/brick/fixture/Chest.gd")

const FloatingLabel = preload("res://script/gui/FloatingLabel.gd")
const ToolBelt = preload("res://script/gui/ToolBelt.gd")
const CharacterRecipeSelectionGui = preload("res://script/gui/CharacterRecipeSelectionGui.gd")
const CraftQueueUi = preload("res://script/gui/CraftQueueUi.gd")
const QuestsUi = preload("res://script/gui/QuestsUi.gd")

const CharacterExhaust = preload("res://scene/CharacterExhaust.tscn")

const CALLBACK_TOOLBELT = "on_toolbelt_change"
const CALLBACK_BACKPACK = "on_backpack_change"


# GLOBAL OBJECT DANGER #########################################################

var god


# STATS ########################################################################

var pickup_grab_range = 41.0
var action_range_squared = pow(350, 2)  # Pixels, squared for perfs
var motion_speed := 260.0               # Pixels/second
var motion_run_multiplier := 2.0        # 30 for cheetahs


# INVENTORY ####################################################################

var hand
var toolbelt
var toolbelt_node
var backpack

var flic_throttler # Floating Labels of Inventory Changes Throttler

var motion := Vector2()
var is_running := false


# LIFECYCLE ####################################################################

func _init(god):
	"""
	Note: we don't use the backpack (yet).
	"""
	self.god = god
	self.hand = Hand.new(god, self)
	self.toolbelt = Inventory.new(10, 2, true, null, self, CALLBACK_TOOLBELT)
	self.backpack = Inventory.new(2, 2, true, null, self, CALLBACK_BACKPACK)
	self.name = "Character"

func _ready():
#	print(String(self.get_instance_id()))
	self._selection_rect = god.get_hud_layer().get_node("SelectionRectangle")
	create_children()
	create_toolbelt_ui_node()
	create_craft_queue_ui()
	god.get_cursor_layer().add_child(hand)
	update_toolbelt_display()
	
	exhaust_lt = CharacterExhaust.instance()
	exhaust_lt.set_position(Vector2(-15,0))
	add_child(exhaust_lt)
	exhaust_rt = CharacterExhaust.instance()
	exhaust_rt.set_position(Vector2(+15,0))
	add_child(exhaust_rt)
	deactivate_exhaust()


# PICKLING #####################################################################

static func from_pickle(god, rick):
	var character = load("res://script/core/Character.gd").new(god)
	
	if rick.has('name'):
		character.name = rick.name
	if rick.has('position'):
		character.position = rick.position
	if rick.has('toolbelt'):
		character.toolbelt = Inventory.from_pickle(
			god, rick.toolbelt, character, CALLBACK_TOOLBELT
		)
	if rick.has('backpack'):
		character.backpack = Inventory.from_pickle(
			god, rick.backpack, character, CALLBACK_BACKPACK
		)
	
	return character

func to_pickle():
	return {
		'toolbelt':  toolbelt.to_pickle(),
		'backpack':  backpack.to_pickle(),
		'position':  position,
		'name':      name,
	}


# SCENE TREE ###################################################################

func queue_free():
	hand.queue_free()
	.queue_free()

var kinematic_body
var cam
var tank_sprite
var body_sprite
var head_sprite 

var initial_headsprite_offset = Vector2(0,-31)
var initial_bodysprite_offset = Vector2(0, -15)
var initial_tanksprite_offset = Vector2(0, -1)


func create_children():
	cam = Camera2D.new()
	cam.name = "Camera"
	cam.anchor_mode = Camera2D.ANCHOR_MODE_DRAG_CENTER
	cam.current = true
	add_child(cam)

	tank_sprite = Sprite.new()
	tank_sprite.name = "Tank"
	tank_sprite.texture = preload("res://data/base/graphics/character/tank_nanogodo_spritesheet.png")
	tank_sprite.vframes = 4
	tank_sprite.hframes = 8
	tank_sprite.centered = true
	tank_sprite.offset = initial_tanksprite_offset
	add_child(tank_sprite)

	body_sprite = Sprite.new()
	body_sprite.name = "Body"
	body_sprite.texture = preload("res://data/base/graphics/character/body_nanogodo_spritesheet.png")
	body_sprite.vframes = 4
	body_sprite.hframes = 8
	body_sprite.centered = true
	body_sprite.position = initial_bodysprite_offset
	add_child(body_sprite)
	
	head_sprite = Sprite.new()
	head_sprite.name = "Head"
	head_sprite.texture = preload("res://data/base/graphics/character/head_nanogodo_spritesheet.png")
	head_sprite.vframes = 4 # Vertical sprites
	head_sprite.hframes = 8 # Horizontal sprites
	head_sprite.centered = true
	head_sprite.position = initial_headsprite_offset
	body_sprite.add_child(head_sprite)
	
	var collision_shape = CollisionShape2D.new()
	collision_shape.shape = CircleShape2D.new()
	collision_shape.shape.radius = 11.5
	add_child(collision_shape)
	
	# F.L.I.C = Floating Labels of Inventory Changes
	flic_throttler = CumulativeThrottler.new(0.888, self, "on_flic")
	flic_throttler.name = "FlicThrottler"
	add_child(flic_throttler)


# SPRITESHEET ##################################################################

const BODY_FIRST_ANGLE = TAU / 4.0  # looks bottom, in this Y-reversed madness
const BODY_ANGLE_COUNT = 4 # BodySprite.hframes
const BODY_FRAME_COUNT = 8 # BodySprite.vframes

# Allows the character to keep the orientation it was facing last
var last_body_spritesheet_vframe = 0

# Keep track of where we are until next frame, by storing fractions of a frame.
# Allows us to slow down spritesheet animations.
var head_frame_advance = 0.0 # up to 1.0
var body_frame_advance = 0.0 # up to 1.0

var cheat_epsilon = 0.0001
func update_body_sprite(motion):
	# /!. Motion has exotic, arbitrary values that may change at any time
	#     like length_squared at 270399.9375 or 67599.984375
	#     Don't rely on motion length too much ;
	#     Though big values make sin() appear somewhat chaotic, and that's good.

	if motion.length() > 1e-5:
		var motion_angle = motion.angle()
		var motion_dot = TAU * motion.dot(Vector2(1,0))
		var speed = motion.length_squared()
		var now = OS.get_ticks_msec()
		
		# Since we only have 4 angles, we want the top and bottom-facing angles
		# to have priority over the left and right when going diagonally.
		var maet = motion_angle * 8 / TAU
		if abs(maet - 3) < cheat_epsilon:
			motion_angle -= cheat_epsilon
		if abs(maet + 3) < cheat_epsilon:
			motion_angle += cheat_epsilon
		
		# Since out spritesheet is a vertical stack of animation rows.,
		# Find the row id of the animation in the spritesheet, from the angle.
		# Works with any number of rows if angles are evenly distributed.
		# Though I'd recommend to have 4 or 8 angles for this.
		var f = (BODY_ANGLE_COUNT + int(round(
			(motion_angle - BODY_FIRST_ANGLE) / (-TAU / BODY_ANGLE_COUNT)
		))) % BODY_ANGLE_COUNT
		last_body_spritesheet_vframe = f
		
		var head_animation_fraction = 11.0  # float pls
		var head_running_modifier = 1.0
		if is_running:
			head_running_modifier = 3.0
		head_frame_advance += head_running_modifier / head_animation_fraction
		var hfai = 0
		if head_frame_advance >= 1.0:
			head_frame_advance = 0.0
			hfai = 1
		
		head_sprite.frame = max(
			f * BODY_FRAME_COUNT,
			(head_sprite.frame + hfai) % ((f+1) * BODY_FRAME_COUNT)
		)
#		head_sprite.rotate(motion.length() * 0.001)
		head_sprite.rotation += -0.000007 * motion_dot
		var head_vroom_vroom = Vector2(0, 0.2777*sin((speed)*0.023*now))
		head_sprite.offset += -0.001 * motion + head_vroom_vroom
		
		# We want the body to be animated at one nth of the frame speed
		var body_animation_fraction = 7.0  # float pls
		var body_running_modifier = 1.0
		if is_running:
			body_running_modifier = 1.5
		body_frame_advance += body_running_modifier / body_animation_fraction
		var bfai = 0
		if body_frame_advance >= 1.0:
			body_frame_advance = 0.0
			bfai = 1
		body_sprite.frame = max(
			f * BODY_FRAME_COUNT,
			(body_sprite.frame + bfai) % ((f+1) * BODY_FRAME_COUNT)
		)
		body_sprite.rotation += -0.000003 * motion_dot
		var body_vroom_vroom = Vector2(0, 0.5*sin(speed*0.01*now))
		body_sprite.offset += -0.0004 * motion + body_vroom_vroom
		
		# Tank treads are simply animated, no fuss
		tank_sprite.frame = max(
			f * BODY_FRAME_COUNT,
			(tank_sprite.frame + 1) % ((f+1) * BODY_FRAME_COUNT)
		)
		
		# Exhaust port is spewing smoke
		activate_exhaust()
	else:
		deactivate_exhaust()
	
	# Slowly roll back to initial position (zero)
	head_sprite.offset   *= 0.9
	body_sprite.offset   *= 0.91
	head_sprite.rotation *= 0.9
	body_sprite.rotation *= 0.9


### PARTICLES ##################################################################

var exhaust_lt
var exhaust_rt

func activate_exhaust():
	exhaust_lt.set_emitting(true)
	exhaust_rt.set_emitting(true)

func deactivate_exhaust():
	exhaust_lt.set_emitting(false)
	exhaust_rt.set_emitting(false)


### INPUT HANDLING #############################################################

func _physics_process(delta):
	
	attract_grabbed_pickups()
	
	# SYSTEM ###################################################################
	
	
	# CURSOR ###################################################################
	
	var cursor_hex = get_tile_under_cursor()
	if null != cursor_hex:
		show_tile_cursor(cursor_hex)
	else:
		hide_tile_cursor()
	
	# CAMERA ###################################################################
	
	# MOVEMENT #################################################################
	
	motion = Vector2(0,0)
	
	if Input.is_action_pressed("move_up"):
		motion += Vector2(0, -1)
	if Input.is_action_pressed("move_bottom"):
		motion += Vector2(0, 1)
	if Input.is_action_pressed("move_left"):
		motion += Vector2(-1, 0)
	if Input.is_action_pressed("move_right"):
		motion += Vector2(1, 0)
	# todo: joysticks
	if Input.is_action_pressed("move_run"):
		is_running = true
	else:
		is_running = false
	
	motion = motion.normalized() * motion_speed
	if is_running:
		motion = motion * motion_run_multiplier
	
	if motion.length() > 1e-5:
		move_and_slide(motion)
		close_fixture_interactive_ui()
	
	
	# BODY PAINTING LIKE HIPPIES ###############################################
	
	update_body_sprite(motion)
	
	# PICKUPS ##################################################################
	
	if Input.is_action_pressed("action_pickup"):
		grab_pickups()
	
	if Input.is_action_just_pressed("action_drop"):
		if not try_dropping_pickup():
			pass  # #audio feedback
			#print("Can't drop! Probably nothing to drop.")
	
	# FIXTURES #################################################################
	
	if not is_any_interface_open():
		if Input.is_action_pressed("action_select_alternate"):
			var ppxy = get_pointer_position()
			var i_can_despawn_a_fixture_here = \
				cursor_hex and \
				not is_any_interface_open() and \
				is_within_action_range(ppxy) and \
				god.board.has_tile_fixtures(cursor_hex)
#				self.hand.is_empty() and \
			if i_can_despawn_a_fixture_here:
				var fixtures = god.board.get_tile_fixtures(cursor_hex)
				var fixture = fixtures[0]
				var fixture_config = god.fixtures[fixture.fixture_type]
				var i_can_probably_despawn = \
					god.pickups.has(fixture_config.slug) and \
					fixture.pickable
				if i_can_probably_despawn:
					var pickup_type = fixture_config.slug
					var pickup = god.spawn_pickup(pickup_type)
					var fixture_pickups = fixture.get_all_pickups()
					var all_pickups = [pickup] + fixture_pickups
					
					if self.has_room_for_pickups(all_pickups):
						self.store_all(all_pickups)
						god.despawn_fixture(fixture)
					else:
						god.snack("I'm full!")  # to throttle
					
#					fixture.decrement_quantum_stability(100)
#					if fixture.is_quantically_unstable():
	
	if Input.is_action_pressed("action_select"):
		var ppxy = get_pointer_position()
		var pointer_is_on_tile = \
			null != cursor_hex and \
			not is_any_interface_open() and \
			is_within_action_range(ppxy) and \
			not is_pointer_on_toolbelt(ppxy)
		
		var i_can_maybe_place_a_fixture_here = \
			pointer_is_on_tile and \
			not self.hand.is_empty() and \
			self.hand.get_held_pickup().is_fixture()
		
		var i_can_maybe_drop_a_pickup_here = \
			pointer_is_on_tile and \
			not self.hand.is_empty() and \
			god.board.has_tile_fixtures(cursor_hex)
		
		if i_can_maybe_place_a_fixture_here:
			if not god.board.has_tile_fixtures(cursor_hex):
				# We can place the fixture in hand on this empty tile
				var fixture_pickup = self.hand.retrieve_one_pickup()
				god.spawn_fixture(
					fixture_pickup.config.fixture,
					cursor_hex, last_direction_used
				)
				god.despawn_pickup(fixture_pickup)
			else:
				# There's something here already, try to re-orient it
				var fixtures_on_tile = god.board.get_tile_fixtures(cursor_hex)
				for fixture in fixtures_on_tile:
					if hand.get_held_pickup().is_fixture(fixture.fixture_type):
						fixture.set_hex_direction(last_direction_used)
		elif i_can_maybe_drop_a_pickup_here:
			var fixtures_on_tile = god.board.get_tile_fixtures(cursor_hex)
			for fixture in fixtures_on_tile:
				# todo : move as much as we can from the hand to the fixture #26
				var hand_pickup = self.hand.get_held_pickup()
				var hand_position = self.hand.position
				if fixture.can_feed_pickup(hand_pickup, hand_position):
					hand_pickup = self.hand.retrieve_pickup() # provide_pickup?
					fixture.feed_pickup(hand_pickup, hand_position) # receive_pickup
	
	# COW POWERS ###############################################################
	
	if Input.is_action_pressed("action_conveyor"):
		if cursor_hex:
			if god.board.has_tile_fixtures(cursor_hex):
				var fixtures = god.board.get_tile_fixtures(cursor_hex)
				for fixture in fixtures:
					if fixture is god.fixtures.belt.script:
						fixture.set_hex_direction(last_direction_used)
			else:
				god.invoke_belt(cursor_hex, last_direction_used)
	
	if Input.is_action_pressed("action_inserter"):
		if cursor_hex:
			if god.board.has_tile_fixtures(cursor_hex):
				var fixtures = god.board.get_tile_fixtures(cursor_hex)
				for fixture in fixtures:
					if fixture is god.fixtures.inserter.script:
						fixture.set_hex_direction(last_direction_used)
			else:
				god.invoke_inserter(cursor_hex, last_direction_used)
	
	if Input.is_action_pressed("action_spawner"):
		if cursor_hex:
			if god.board.has_tile_fixtures(cursor_hex):
				var fixtures = god.board.get_tile_fixtures(cursor_hex)
				for fixture in fixtures:
					if fixture is god.fixtures.pickup_spawner.script:
						fixture.set_hex_direction(last_direction_used)
			else:
				god.invoke_spawner(cursor_hex, last_direction_used, "erythrocyte")
	
#	if selecting_blueprint:
#		if Input.is_action_pressed("action_pickup"):
#			grab_pickups()

# Only one fixture interactive UI may be opened at a time.
# The UIs rely on this variable as well.
var fixture_with_open_gui



var blueprint_rectangle = null  # first corner position (Vector2)
var capturing_blueprint = false
var selecting_blueprint = false
var last_direction_used = Vector2(1, 0)  # the default everywhere (i hope)
var conveyors_count = 0
func _input(ev):
	var zoom_min = 0.11
	var zoom_max = 6.66
	var zoom_speed = 1.1
	
	# Special cases come first
	
	if capturing_blueprint:
		if Input.is_action_just_pressed("action_select"):
			selecting_blueprint = true
			var p = get_pointer_position()
			blueprint_rectangle = p
			hide_tile_cursor()
		if Input.is_action_just_released("action_select"):
			var p = get_pointer_position()
			var top_left = Vector2(
				min(p.x, blueprint_rectangle.x),
				min(p.y, blueprint_rectangle.y)
			)
			var dimensions = Vector2(
				abs(p.x - blueprint_rectangle.x),
				abs(p.y - blueprint_rectangle.y)
			)
			var selection_rect = Rect2(top_left, dimensions)
			var hexs = god.board.get_hexs_in_rect(selection_rect)
			
			if hexs:
#				print("Selected %d tiles." % hexs.size())
				var data = god.blueprint.serialize_fixtures_on(
					hexs, god.board.pix_to_hex(top_left+dimensions/2.0)
				)
				var token = god.blueprint.compress_string(data)
#				print("Exchange Token (boring): %s" % token)
#				print("Original: ",god.blueprint.decompress_string(token))
				var token_pretty = god.blueprint.compress_string_pretty(data)
				print("Exchange Token (pretty): %s" % token_pretty)
				print("Original: ",god.blueprint.decompress_string_pretty(token_pretty))
			
			selecting_blueprint = false
			capturing_blueprint = false
			hide_selection_rect()
		if Input.is_key_pressed(KEY_ESCAPE):
			selecting_blueprint = false
			capturing_blueprint = false
			hide_selection_rect()
	
	if ev is InputEventMouseMotion:
		var p = get_pointer_position()
#		var xy = god.board.hex_to_pix(hex)
		
		if selecting_blueprint:
			var top_left = Vector2(
				min(p.x, blueprint_rectangle.x),
				min(p.y, blueprint_rectangle.y)
			)
			var dimensions = Vector2(
				abs(p.x - blueprint_rectangle.x),
				abs(p.y - blueprint_rectangle.y)
			)
			show_selection_rect(top_left, dimensions)

	if ev is InputEventMouseButton:
		
		if Input.is_action_pressed("action_zoomout"):
			var z = cam.get_zoom()*Vector2(zoom_speed,zoom_speed)
			if z.length_squared() > 2 * pow(zoom_max,2):
				z = Vector2(zoom_max, zoom_max)
			cam.set_zoom(z)
		if Input.is_action_pressed("action_zoomin"):
			var z = cam.get_zoom()/Vector2(zoom_speed,zoom_speed)
			if z.length_squared() < 2 * pow(zoom_min,2):
				z = Vector2(zoom_min, zoom_min)
			cam.set_zoom(z)

	### EXPERIMENTAL BLUEPRINTS ################################################
	
	if Input.is_action_just_pressed("action_capture_blueprint"):
#		Input.set_custom_mouse_cursor(null, Input.CURSOR_CROSS)
		if capturing_blueprint:
			capturing_blueprint = false
		else:
			hide_tile_cursor()
			capturing_blueprint = true


	#### FREE HAND #############################################################

	if Input.is_action_just_pressed("action_free_hand"):
		get_hand().clear(false)


	### ROTATE UNDER CURSOR ####################################################

	var action_rotate = 0
	if Input.is_action_just_pressed("action_rotate_clockwise"):
		action_rotate = 1
#	if Input.is_action_just_pressed("action_rotate_clockwise_twice"):
#		action_rotate = 2
	if Input.is_action_just_pressed("action_rotate_trigwise"):
		action_rotate = -1
#	if Input.is_action_just_pressed("action_rotate_trigwise_twice"):
#		action_rotate = -2
#	if Input.is_action_just_pressed("action_rotate_invert"):
#		action_rotate = 3
	if action_rotate != 0:
		# Rotation is clockwise
		var p = get_pointer_position()
		var hex = god.board.pix_to_hex(p)
		if god.board.has_tile_fixtures(hex):
			var fixture = god.board.get_tile_fixture(hex)
			var directions = [  # clockwise
				Vector2( 1, 0), Vector2( 0, 1), Vector2(-1, 1),
				Vector2(-1, 0), Vector2( 0,-1), Vector2( 1,-1)
			]
			var chd = fixture.get_hex_direction()
			assert(chd in directions)
			var i = directions.find(chd)
			
			var new_direction = directions[(i+action_rotate)%directions.size()]
			last_direction_used = new_direction
			
			fixture.set_hex_direction(new_direction)
	
	if Input.is_action_just_pressed("action_pause"):
		if is_recipe_selection_ui_open():
			close_recipe_selection_ui()
		elif fixture_with_open_gui:
			close_fixture_interactive_ui()
		else:
			god.game.pause_game()
	
	# Note that these toolbelt keyboard events are always triggered,
	# even if we define this logic in _unhandled_input
	# Also note that if we're moving the mouse around fast,
	# sometimes these actions are ignored ; no idea why yet.
	for i in range(12):
		if Input.is_action_just_pressed("toolbelt_%02d" % i):
			var slot = Vector2(i+1, 1)
			if get_hand().is_empty():
				get_hand().grab(get_toolbelt(), slot)
			else:
				if get_hand().is_holding(get_toolbelt(), slot):
					get_hand().clear()
				else:
					get_hand().clear()
					get_hand().grab(get_toolbelt(), slot)
	
	return # end of _input


### MAP INPUTS #################################################################

func _unhandled_input(ev):
	"""
	Events that were not caught by any GUI.
	"""
	if Input.is_action_just_pressed("debug_dump_under_cursor"):
		var p = get_pointer_position()
		var hex = god.board.pix_to_hex(p)
		var fixture = god.board.get_tile_fixture(hex)
		print("Tile %s" % hex)
		if fixture:
			print(fixture.get_dump())

	if Input.is_action_just_pressed("toggle_crafting_interface"):
		if not is_recipe_selection_ui_open():
			open_recipe_selection_ui()
		else:
			close_recipe_selection_ui()

	if Input.is_action_just_pressed("toggle_quests_ui"):
		if not is_quests_ui_open():
			open_quests_ui()
		else:
			close_quests_ui()
	
	if Input.is_action_just_pressed("action_select"):
		var p = get_pointer_position()
		var hex = god.board.pix_to_hex(p)
		var fixture = god.board.get_tile_fixture(hex)
		
		if self.hand.is_empty():
			if fixture:
				if Input.is_action_pressed("action_select_modifier_all"):
					if fixture is Chest:
						get_toolbelt().store_all(fixture.inventory)
				else:
					close_fixture_interactive_ui()  # if any
					open_fixture_interactive_ui(fixture)
		else:
			# This is now handled in the process, so that we can maintain
			# the mouse click and "paint" around with our hand.
			pass


### ACCESSORS ##################################################################

func get_toolbelt():
	return self.toolbelt

func get_toolbelt_gui():
	return self.toolbelt_node

func get_hand():
	return self.hand


### QUESTS UI ##################################################################

var quests_ui

func is_quests_ui_open():
	return null != quests_ui

func open_quests_ui():
	assert(null == quests_ui)
	quests_ui = QuestsUi.new(god)
	god.get_fixed_hud_layer().add_child(quests_ui)
	quests_ui.connect("tree_exited", self, "on_quests_ui_exit")

func on_quests_ui_exit():
	quests_ui = null

func close_quests_ui():
	quests_ui.close()
	quests_ui = null # redundant


### FIXTURES INTERACTIVE UI ####################################################

func close_fixture_interactive_ui(fixture=null):
	if null == fixture and null == fixture_with_open_gui:
		return  # possibly faster as this is true 99.9% of the time
	if not fixture:
		fixture = fixture_with_open_gui
	if fixture:
		fixture.close_interactive_ui()
	if fixture == fixture_with_open_gui:
		fixture_with_open_gui = null

func open_fixture_interactive_ui(fixture):
	if recipe_selection_ui:
		close_recipe_selection_ui()
	fixture.open_interactive_ui()
	fixture_with_open_gui = fixture

func is_fixture_gui_open(of_fixture=null):
	if null == of_fixture:
		return null != fixture_with_open_gui
	else:
		return of_fixture == fixture_with_open_gui

func is_any_interface_open():
	return \
		null != fixture_with_open_gui or \
		null != recipe_selection_ui


### HELPERS ####################################################################

func get_pointer_position():  # will help with joypads?
	return get_global_mouse_position()

func get_tile_under_cursor():
	var p_xy = get_pointer_position()
	var hex = god.board.pix_to_hex(p_xy)  # origin of closest hex, remember
	var hex_xy = god.board.hex_to_pix(hex)
	if (p_xy - hex_xy).length_squared() < pow(god.board.hex_lattice_size*0.77,2):
		return hex
	return null

func is_within_action_range(xy):
	return self.action_range_squared > (self.position - xy).length_squared()

func is_pointer_on_toolbelt(ppxy=null):
	if null == ppxy:
		ppxy = get_pointer_position()
	var cppxy = global_to_viewport(ppxy)
	if get_toolbelt_gui().get_rect().has_point(cppxy):
		return true
	return false

func global_to_viewport(xy):
	return get_viewport_transform().xform(xy)


### UI #########################################################################

onready var _hex_cursor = god.get_feedback_layer().get_node("SingleTileCursor")
func show_tile_cursor(at_hex):
	_hex_cursor.position = god.board.hex_to_pix(at_hex)
	_hex_cursor.visible = true
func hide_tile_cursor():
	_hex_cursor.visible = false
	
var _selection_rect 
func show_selection_rect(top_left, dimensions):
	_selection_rect.position = top_left
	_selection_rect.scale = dimensions
	_selection_rect.visible = true
func hide_selection_rect():
	_selection_rect.visible = false


### FLOATING LABELS ############################################################

# Floating labels are the small one-liners displayed above the character.

func spawn_floating_label(message):
	var fl = FloatingLabel.new(god, message)
	fl.theme = preload("res://data/base/graphics/ui/floating_label_theme.tres")
	add_child(fl)


### INVENTORY ##################################################################

func on_flic(pickup_type, count):
	var config = god.pickups[pickup_type]
	spawn_floating_label("+%d %s" % [count, config.name])

func on_toolbelt_change(event, pickup_type, quantity):
	update_toolbelt_display()
	if event == Inventory.EVENT_PICKUPS_ADDED:
		flic_throttler.throttle(pickup_type, quantity)

func on_backpack_change(event, pickup_type, quantity):
	print("Backpack change: %d %s" % [quantity, pickup_type])
	assert(not "done, yet")


var attracted_pickups = []

func grab_pickups():
	var grab_range = pickup_grab_range
	var grab_range_pow = pow(grab_range, 2)
	var x = position.x
	var y = position.y

	for pickup in god.board.get_pickups_on_grid_in_circle(position, grab_range):
		assert(pickup.pickable)
#		if not pickup.pickable:
#			continue
		
		if has_room_for(pickup.pickup_type):
			attracted_pickups.append(pickup)
			pickup._grabbed_vel = 0.01
			god.board.forget_pickup(pickup)
	
	for fixture in god.board.get_fixtures_in_circle(position, grab_range):
		if fixture is god.fixtures.belt.script:
			for pickup in fixture.get_all_pickups():
				if (position-pickup.position).length_squared() > grab_range_pow:
					continue
				if has_room_for(pickup.pickup_type):
					attracted_pickups.append(pickup)
					pickup._grabbed_vel = 0.01
					fixture.free_pickup(pickup)

func has_room_for(pickup_type=null, amount=1):
	# also, we need to check the backpack somehow
	return toolbelt.has_room_for(pickup_type, amount)

func has_room_for_pickup(pickup):
	return has_room_for(pickup.pickup_type, pickup.stack)

func has_room_for_pickups(pickups):
	# also, we need to check the backpack somehow
	return toolbelt.has_room_for_pickups(pickups)

func store_all(pickups):
	# also, we need to check the backpack somehow
	for pickup in pickups:
		store(pickup)
	if recipe_selection_ui:
		recipe_selection_ui.update_display(get_recipe_labels())

func store(pickup):
	if pickup.get_parent():
		pickup.get_parent().remove_child(pickup)
	# also, we need to check the backpack somehow
	toolbelt.store(pickup)
	# refactor: use signals
	if recipe_selection_ui:
		recipe_selection_ui.update_display(get_recipe_labels())

func retrieve(pickup_type, quantity):
	# also, we need to check the backpack somehow
	var p = toolbelt.retrieve(pickup_type, quantity)
	
	# refactor: use signals
	if recipe_selection_ui:
		recipe_selection_ui.update_display(get_recipe_labels())
	
	return p

func attract_grabbed_pickups():
	if not attracted_pickups:
		return
	var remainder = []
	for pickup in attracted_pickups:
		if not pickup.pickable:
			# The pickup we were attracting was grabbed by an inserter, skip.
			continue
		
		pickup.position = pickup.position.linear_interpolate(
			self.position,
			pickup._grabbed_vel
		)
		pickup._grabbed_vel = min(1, pickup._grabbed_vel + 0.0222)
		
		var d2 = (pickup.position-position).length_squared()
		if d2 < 32:  # 5 pixels away from player's origin
			if has_room_for_pickup(pickup):
				if pickup.get_parent():
					pickup.get_parent().remove_child(pickup)
				else:
					print("/!. SMELL: Grabbed %s had no scene parent" % pickup.name)
				store(pickup)
#				update_toolbelt_display()
#				flic_throttler.throttle(pickup.pickup_type, pickup.stack)
			else:
				if not god.board.try_drop(pickup, pickup.position):
					remainder.append(pickup)
		else:
			remainder.append(pickup)
	
	attracted_pickups = remainder

func try_dropping_pickup():
	var pickup
	var inventory
	var inventory_slot
	var drop_position = position
	
	if self.hand.is_empty():
		return false
	
	inventory = self.hand.held_inventory
	inventory_slot = self.hand.held_inventory_slot
	pickup = inventory.retrieve_from_slot(inventory_slot, 1, false)
	drop_position = get_global_mouse_position()
	
	pickup.position = drop_position
	
	if not god.board.try_drop(pickup, drop_position):
		print("Cannot drop my %s on xy%s" % [pickup.name, drop_position])
		inventory.store(pickup, inventory_slot, false)
		return false
	else:
		hand.clear_if_necessary()
		inventory.fire_retrieve_callback(pickup.pickup_type, 1)
	return true


### TOOLBELT ###################################################################

func create_toolbelt_ui_node():
	var toolbelt_parent_node = god.get_fixed_hud_layer()
	toolbelt_node = ToolBelt.new(god, self)
	toolbelt_node.name = "ToolBelt"
	toolbelt_parent_node.add_child(toolbelt_node)

func update_toolbelt_display():
	assert(toolbelt_node)
	toolbelt_node.update_display()
#	print("CRAFT ",count_possible_crafts_of_recipe("erythrocyte_o2"))


### CRAFTING ###################################################################

# Example config in god
#const recipes = {
#	'erythrocyte_o2': {
#		'slug': 'erythrocyte_o2',  # not used (yet), but maybe we'll want this
#		'time': 10000, # ms
#		'intakes': [
#			{
#				'pickup_type': 'erythrocyte',
#				'quantity': 1
#			},{
#				'pickup_type': 'oxygen',
#				'quantity': 2
#			}
#		],
#		'outakes': [
#			{
#				'pickup_type': 'erythrocyte_o2',
#				'quantity': 1
#			}
#		],
#	},
#}

var recipe_selection_ui
func open_recipe_selection_ui():
	if fixture_with_open_gui:
		close_fixture_interactive_ui()
	assert(not recipe_selection_ui)
	recipe_selection_ui = CharacterRecipeSelectionGui.new(
		god, "Crafting",
		god.recipes,
		get_recipe_labels()
	)
	
	god.get_interactive_hud_layer().add_child(recipe_selection_ui)

func close_recipe_selection_ui():
	if recipe_selection_ui:
		recipe_selection_ui.close()
		recipe_selection_ui = null

func is_recipe_selection_ui_open():
	return null != self.recipe_selection_ui

func count_possible_crafts_of_recipe(recipe_slug, inventory2=null):
	"""
	Todo:
		- apply recursion to really find how much you can craft.
	"""
	if null == inventory2:
		inventory2 = get_toolbelt()
	var inventory = inventory2.copy()  # let's work with a copy, so we can trash it
	var recipe = god.recipes[recipe_slug]
	var intakes_count = recipe.intakes.size()
	var count = 0
	
	var intakes_each_count = Array()  # [0] * recipe.intakes.size()
	for intake_index in range(intakes_count):
		intakes_each_count.append(0)
	for slot in inventory.slots:
		var pickup = inventory.slots[slot]
		for intake_index in range(intakes_count):
			var intake = recipe.intakes[intake_index]
			if pickup.pickup_type == intake.pickup_type:
				intakes_each_count[intake_index] += pickup.stack
				break
	
	# Ignore already reserved pickups in the current queue
	for intake_index in range(intakes_count):
		var reserved_amount = 0
		var pickup_type = recipe.intakes[intake_index].pickup_type
		if crafting_reserved_pickups.has(pickup_type):
			reserved_amount = crafting_reserved_pickups[pickup_type]
		intakes_each_count[intake_index] -= reserved_amount
	
	# Factor in the needed quantity of each intake
	for intake_idx in range(intakes_count):
		intakes_each_count[intake_idx] /= recipe.intakes[intake_idx].quantity
	
	# Find the weak link, that's how much we can make
	# count = ArrayUtils.min(intakes_each_count)
	intakes_each_count.sort()     # kinda expensive, to get the minimum
	count = intakes_each_count[0] # <- here it is
	count = max(0, count)  # gamers tend to try monkey business
	
	# Destroy the inventory
	inventory.destroy()
	
	return count

func can_craft_recipe(recipe_slug):
	# overly expensive, but simple
	return count_possible_crafts_of_recipe(recipe_slug) > 0

func get_recipe_labels():
	var recipe_labels = Dictionary()
	for i in range(god.recipes.keys().size()):
		recipe_labels[god.recipes.keys()[i]] = str(count_possible_crafts_of_recipe(god.recipes.keys()[i]))
	return recipe_labels

var crafting_recipe_queue = Array()
var crafting_reserved_pickups = Dictionary()  # pickup_type => amount
func queue_craft_recipe(recipe_slug):
	assert(god.recipes.has(recipe_slug))
	if can_craft_recipe(recipe_slug):
		var recipe = god.recipes[recipe_slug]
		# Reserve intake pickups
		for intake in recipe.intakes:
			var pickup_type = intake.pickup_type
			if not crafting_reserved_pickups.has(pickup_type):
				crafting_reserved_pickups[pickup_type] = 0  # lazy fill
			crafting_reserved_pickups[pickup_type] += intake.quantity
		
		crafting_recipe_queue.append(recipe_slug)
		craft_queue_ui.add_to_queue(recipe_slug, 1)
		
#		var cqs = CraftQueueSlot.new(god, recipe_slug, 1)
#		get_node("/root/App/Game/World/InteractiveHudLayer").add_child(cqs)
		return true
	else:
		printerr("Cannot queue craft '%s'. Not enough intakes." % recipe_slug)
		return false

var craft_queue_ui
func create_craft_queue_ui():
	assert(not craft_queue_ui)
	craft_queue_ui = CraftQueueUi.new(god, self)
	god.get_interactive_hud_layer().add_child(craft_queue_ui)

func update_craft_queue_ui(progress=0.0):
#	if not craft_queue_display:
#		craft_queue_display = CraftQueueSlot.new()
	assert(craft_queue_ui)
	craft_queue_ui.update_display(progress)


func is_crafting():
	return not is_not_crafting()

func is_not_crafting():
	return crafting_recipe_queue.empty()

# The recipe currently being crafted
#var current_recipe
var current_craft_progress = 0.0

func _process(delta):
	if is_not_crafting():
		return
	
	var current_craft_recipe = god.recipes[crafting_recipe_queue[0]]

	update_craft_queue_ui(current_craft_progress)

	if current_craft_progress < 1.0:
		# This yields minor numerical instabilities, but we don't care \m/
		current_craft_progress += delta * 1000.0 / current_craft_recipe.time
	else:
		#print("Craft progress = 100%")
		# Whatever happens, we need to stop reserving the intake pickups
		# And we do it first to be able to call can_craft_recipe()
		for intake in current_craft_recipe.intakes:
			assert(crafting_reserved_pickups.has(intake.pickup_type))
			crafting_reserved_pickups[intake.pickup_type] -= intake.quantity
			assert(crafting_reserved_pickups[intake.pickup_type] >= 0)
			# ↑ weak assertion, maybe use max(0,)
		
		var crafted_pickups = Array()
		for outake in current_craft_recipe.outakes:
			crafted_pickups.append(god.spawn_pickup(outake.pickup_type, outake.quantity))
		
		var i_have_enough_room = has_room_for_pickups(crafted_pickups)
		var i_can_still_craft = can_craft_recipe(current_craft_recipe.slug)
		
		if not i_can_still_craft:
			god.snack("I don't have enough resources to craft that!")
			for crafted_pickup in crafted_pickups:
				god.despawn_pickup(crafted_pickup)
		elif not i_have_enough_room:
			god.snack("My belly is full already!")
			for crafted_pickup in crafted_pickups:
				god.despawn_pickup(crafted_pickup)
		else:
			# Remove the intakes
			for intake in current_craft_recipe.intakes:
				# Not sure what will happen on retrieve() in this case.
				# We'll need this at some point ; feel free to code on!
				assert(intake.quantity <= god.pickups[intake.pickup_type].stack)
				
				var intake_pickup = retrieve(intake.pickup_type, intake.quantity)
				god.despawn_pickup(intake_pickup)
			# Add the outakes
			for crafted_pickup in crafted_pickups:
				store(crafted_pickup)
		
		current_craft_progress = 0
		crafting_recipe_queue.pop_front()
#	update_craft_display()
	
