extends "res://script/brick/fixture/Inserter.gd"


func init(god, tile=Vector2(0,0), to_intake=Vector2(1,0), to_outake=null):
	.init(god, tile, to_intake, to_outake)
	self.fixture_type = "inserter_long"
	self.arm_range = 2
	arm_local_start = Vector2(24, 0)
	arm_local_end = Vector2(107, 0)

func _ready():
	set_texture(preload("res://data/base/graphics/fixture/inserter_long/inserter_long_body.png"))
	arm_sprite.set_texture(preload("res://data/base/graphics/fixture/inserter_long/inserter_long_arm.png"))
	hand_sprite.set_texture(preload("res://data/base/graphics/fixture/inserter_long/inserter_long_hand.png"))
