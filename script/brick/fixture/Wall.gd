extends "res://script/core/Fixture.gd"
#class_name FixtureWall

func init(god, tile=Vector2(0,0), direction=Vector2(1, 0)):
	"""
	A wall is an unpassable structure (for characters).
	We're leveraging Godot's physics engine,
	so that means this node creates two children.
	
	Watch out for unpickle() as well if you edit the _init() API signature.
	"""
	.init(god, tile, direction)
	self.fixture_type = "wall"

func _ready():
	set_texture(preload("res://data/base/graphics/fixture/wall/wall.png"))
	set_centered(true)
	set_offset(Vector2(0, -11))
	var _scale = 1.5
	apply_scale(Vector2(_scale, _scale))
	add_circular_collidable_body(0.9 / _scale)


### PICKLING ###################################################################

static func from_pickle(god, rick):
	var wall = load("res://script/brick/fixture/Wall.gd").new()
	wall.init(
		god,
		rick.hex_position,
		rick.hex_direction
	)
	
	inject_pickle(wall, rick)
	
	return wall

# Nothing special here
#func to_pickle(relative_to=Vector2(0,0), for_blueprint=false):
#	return Utils.merge(.to_pickle(relative_to, for_blueprint), {
#	})


### PICKUP EXCHANGE INTERFACE ##################################################

# … Nope.


### PAINTING ###################################################################

# Choose a texture hframe given neighbors


### DEBUG ######################################################################

func get_dump():
	var desc = "%s\n" % self.name
	desc += "  at %s\n" % hex_position
	desc += "Pickled :\n%s\n" % to_pickle()
	return desc

