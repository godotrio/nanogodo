extends "res://script/core/Fixture.gd"

# Conveyor Belts are the blood and bones of a factory game.
# They actually *look* like blood and bones, but that's coincidental.

################################################################################

const BIGINT = 4294967294  # 2<<31 - 2
const SQRT32 = 0.8660254   # sqrt(3)/2.0

# internal pickup physics
const PICKUP_SIZE = 12.0    # in steps below, the pickup width on the lane
const PICKUP_STEP = 1/32.0  # fraction of half the width of the belt

const Pickup = preload("res://script/core/Pickup.gd")


# Vector2(q,r) : unit vector in axial coordinates to the tile the belt looks at.
#var to_next # deprecated redundant with hex_direction


func init(god, tile, direction=Vector2(1,0)):
	.init(god, tile, direction)
	self.fixture_type = 'belt'
	#self.to_next = direction
	
	recompute_lanes()
	recompute_waypoints()
	
#	for lane in lanes:
#		print(lane)


func _ready():
	assert(god.board)
	
	#set_texture(preload("res://data/base/graphics/fixture/belt/belt_(-1,0).png"))
	set_hframes(16)
	set_scale(Vector2(0.975,0.967))  # magic number
	
	reposition_sprite()
	recompute_texture()

### PICKLING ###################################################################

static func from_pickle(god, rick):
	var belt = load("res://script/brick/fixture/Belt.gd").new()
	belt.init(
		god,
		rick.hex_position,
		rick.hex_direction
	)
	
	inject_pickle(belt, rick)
	
	if rick.has('lanes'):
		assert(rick.lanes.size() == belt.lanes.size())
		for pickled_lane_i in range(rick.lanes.size()):
			var pickled_lane = rick.lanes[pickled_lane_i]
			var pickups = Array()
			for pickled_pickup in pickled_lane.pickups:
				pickups.append(Pickup.from_pickle(god, pickled_pickup))
			for pickup_i in range(pickups.size()):
				var pickup = pickups[pickup_i]
				belt.lanes[pickled_lane_i].order.append(pickup)
				belt.lanes[pickled_lane_i].data[pickup] = pickled_lane.data[pickup_i]
	
	return belt

func to_pickle(relative_to=Vector2(0,0), for_blueprint=false):
	var pickle = Utils.merge(.to_pickle(relative_to, for_blueprint), {})
	if not for_blueprint:
		var pickled_lanes = Array()
		for lane in lanes:
			var pickled_pickups = Array()
			var pickled_data = Array()
			for pickup in lane.order:
				pickled_pickups.append(pickup.to_pickle())
				pickled_data.append(lane.data[pickup])
			pickled_lanes.append({
				'pickups': pickled_pickups,
				'data': pickled_data,
			})
		pickle['lanes'] = pickled_lanes
	
	return pickle


### ROTATION ###################################################################

# Override parent method
func set_hex_direction(hex):
	self.hex_direction = hex
#	self.to_next = hex
	#recompute_intake()
	recompute_waypoints()
	reposition_sprite()
	recompute_texture()


### MACHINERY ##################################################################

var current_machinery_tick = -1 # ever-incrementing integer
func on_machinery_tick(progress, current_machinery_tick):
	self.current_machinery_tick = current_machinery_tick # first
	
	if progress == 1.0 and OS.is_debug_build():
		check_internal_statuses()
	
	advance_pickups_on_belt()


### LANES ######################################################################

var lanes
var xx
var yy

const LANES_TO_NEXT = [0] # indices of lanes going to another belt
const LANES_INDICES = [0,1,2,3,4,5,6]
const INTAKES_LANES_MAPPING = [0,6,4,2,3,5]
func recompute_lanes():
	# Hexagon side
	xx = god.board.hex_lattice_size
	# CD / CB
	yy = 0.6
	# We define seven (but any number, really) of lanes, in order of processing.
	# - C is the Center of the hexagon tile
	# - B is the middle of the hexagon's Outake side
	# - D is somewhere between C and B, defined by self.yy
	# - I<n> are the middles of the 5 Intakes sides (0≤n≤4), clockwise from B
	# Each lane consists of
	#   data: Pickup => Dict with position between 0 and 1 and idle frames
	#   order: Array of Pickups in the lane (the above keys, *ordered*)
	#   length: In pixels, the length of the lane
	#   ratio: fraction of half the width of the belt
	#   treshold: When pickup progress is above this treshold, will check confluents
	#             deprecated, compute this instead
	#   confluents: Array of indices of lanes with whom this lane has confluence
	#   next: Index of the lane after this one, if any.
	#         Only the outake lanes do not have this property.
	# Waypoints are in pixel land, and are computed by recompute_waypoints()
	self.lanes = [
		{
			'name':       'DB  Outake', # 0
			'start':      'D',
			'end':        'B',
			'wp_start':   null,
			'wp_end':     null,
			'length':     SQRT32 * xx * (1 - yy),
			'ratio':      1 - yy,
			'data':       Dictionary(),
			'order':      Array(),
			'treshold':   1.1, # disabled
			'confluents': [],
		},
		{
			'name':       'CD  Internal', # 1
			'start':      'C',
			'end':        'D',
			'wp_start':   null,
			'wp_end':     null,
			'length':     SQRT32 * xx * yy,
			'ratio':      yy,
			'data':       Dictionary(),
			'order':      Array(),
			'treshold':   0.5,
			'confluents': [5, 6],
			'next':       0,
		},
		{
			'name':       'I²C Back Intake', # 2
			'start':      'I2',
			'end':        'C',
			'wp_start':   null,
			'wp_end':     null,
			'length':     SQRT32 * xx,
			'ratio':      1.0,
			'data':       Dictionary(),
			'order':      Array(),
			'treshold':   0.64,
			'confluents': [3, 4],
			'next':       1,
		},
		{
			'name':       'I³C Back Left Intake', # 3
			'start':      'I3',
			'end':        'C',
			'wp_start':   null,
			'wp_end':     null,
			'length':     SQRT32 * xx,
			'ratio':      1.0,
			'data':       Dictionary(),
			'order':      Array(),
			'treshold':   0.58,
			'confluents': [2, 4],
			'next':       1,
		},
		{
			'name':       'I¹C Back Right Intake', # 4
			'start':      'I1',
			'end':        'C',
			'wp_start':   null,
			'wp_end':     null,
			'length':     SQRT32 * xx,
			'ratio':      1.0,
			'data':       Dictionary(),
			'order':      Array(),
			'treshold':   0.58,
			'confluents': [2, 3],
			'next':       1,
		},
		{
			'name':       'I⁴D Front Left Intake', # 5
			'start':      'I4',
			'end':        'D',
			'wp_start':   null,
			'wp_end':     null,
			'length':     SQRT32 * xx * sqrt(pow(yy, 2) - yy + 1),
			'ratio':      sqrt(pow(yy, 2) - yy + 1),
			'data':       Dictionary(),
			'order':      Array(),
			'treshold':   0.58,
			'confluents': [1, 6],
			'next':       0,
		},
		{
			'name':       'I⁰D Front Right Intake', # 6
			'start':      'I0',
			'end':        'D',
			'wp_start':   null,
			'wp_end':     null,
			'length':     SQRT32 * xx * sqrt(pow(yy, 2) - yy + 1),
			'ratio':      sqrt(pow(yy, 2) - yy + 1),
			'data':       Dictionary(),
			'order':      Array(),
			'treshold':   0.58,
			'confluents': [1, 5],
			'next':       0,
		},
	]


func get_lanes_indices(): # so we can try alternating intakes later
	return LANES_INDICES

func get_lane_ratio(lane):
	return lane.ratio
#	return (lane.length) / (self.xx * SQRT32)

func get_intake_lane_from(direction):
	assert(direction in god.board.HEX_DIRECTIONS)
	var i = god.board.HEX_DIRECTIONS.find(direction)
	var j = god.board.HEX_DIRECTIONS.find(self.hex_direction)
	return self.lanes[INTAKES_LANES_MAPPING[(i-j+6)%6]] #                       HALP I'M TRAPPED IN A VIDEO GAME FACTORY

func get_progress_of_last_on_lane(lane):
	return lane.data[lane.order[lane.order.size() - 1]].progress

func find_closest_lane(xy):
	var epsilon = 1.0
	var closest = 0
	var closest_distance = BIGINT # INF?
	
	for i in get_lanes_indices():
		var d2 = (xy - lanes[i].wp_start).length_squared()
		if d2 < epsilon:
			# We're close enough, that's the one. No need for extra computation.
			return i
		elif d2 < closest_distance:
			closest_distance = d2
			closest = i
	
	return closest

func is_lane_empty(lane):
	return lane.order.empty()

func lane_to_global(lane, progress):
	return progress * get_lane_ratio(lane)

func global_to_lane(lane, progress):
	return progress / get_lane_ratio(lane)

func can_feed_lane(lane):
	if lane.order.empty():
		return true
	var last_pickup_prg = get_progress_of_last_on_lane(lane)
	if last_pickup_prg < global_to_lane(lane, PICKUP_SIZE * PICKUP_STEP):
		 return false
	return true

func move_pickup_to_other_lane(from_lane, pickup, to_lane):
	# Assume the target lane has room
	var overflow = global_to_lane(
		to_lane,
		lane_to_global(from_lane, from_lane.data[pickup].overflow)
	)
	if not is_lane_empty(to_lane):
		overflow = max(0, min(
			overflow,
			get_progress_of_last_on_lane(to_lane) \
			- \
			global_to_lane(to_lane, PICKUP_SIZE * PICKUP_STEP)
		)) 
	
	# free_pickup(pickup, from_lane) or, better,
	# forget_pickup(pickup, from_lane)
	from_lane.order.erase(pickup)
	from_lane.data.erase(pickup)
	
	to_lane.order.append(pickup)
	to_lane.data[pickup] = {
		'progress': overflow,
		'overflow': 0,
		'idle':     0,
	}
	
	assert(overflow >= 0)
	if overflow > 0:
		reposition_pickup(pickup, to_lane, overflow)


### PICKUPS ####################################################################

func display_pickup(pickup):
	var parent = pickup.get_parent()
	if parent != god.get_pickups_layer():
		if parent:
			parent.remove_child(pickup)
		god.get_pickups_layer().add_child(pickup)

func free_pickup(pickup):
	#print("Freeing pickup %s from %s" % [pickup.name, self.name])
	var found = false
	for i in get_lanes_indices():
		if lanes[i].data.has(pickup):
			lanes[i].data.erase(pickup)
			lanes[i].order.erase(pickup)
			found = true
			break
	assert(found)  # strict mode

func get_all_pickups():
	var all_pickups = Array()
	for i in get_lanes_indices():
		for pickup in lanes[i].order:
			all_pickups.append(pickup)
	return all_pickups

func get_one_pickup():
	for i in get_lanes_indices():
		for pickup in lanes[i].order:
			return pickup

#func get_pickup_list():  # deprecated alias
#	print("deprecated call to get_pickup_list()")
#	return get_all_pickups()

func count_pickups():
	var count = 0
	for i in get_lanes_indices():
		count += lanes[i].order.size()
	return count

func has_pickup(pickup):
	for i in get_lanes_indices():
		if pickup in lanes[i].order:
			return true
	return false


func can_feed_pickup(pickup, xy):
	if count_pickups() < 3:
		return true
	var i = find_closest_lane(xy)
	if can_feed_lane(lanes[i]):
		return true
	else:
		return false


func feed_pickup(pickup, xy):
	#print("Feeding pickup %s to %s…" % [pickup.name, self.name])
	display_pickup(pickup)
	
	assert(can_feed_pickup(pickup, xy))
	assert(not has_pickup(pickup))
	
	var lane_index = find_closest_lane(xy)
	lanes[lane_index].order.append(pickup)
	lanes[lane_index].data[pickup] = {
		'progress': 0,
		'overflow': 0,
		'idle':     0,
	}


func can_provide_pickup(pickup_type=null, amount=1):
	assert(pickup_type == null)  # to implement
	assert(amount == 1)  # to implement
	for lane in lanes:
		if lane.data.size() > 0:
			return true
	return false

func provide_pickup(pickup_type=null, amount=1):
	assert(pickup_type == null)  # to implement
	assert(amount == 1)  # to implement
	var provided_pickup
	for i in get_lanes_indices():
		var lane = lanes[i]
		if lane.order.size() > 0:
			assert(lane.data.size() > 0)
			provided_pickup = lane.order.pop_front()
			assert(lane.data.has(provided_pickup))
			lane.data.erase(provided_pickup)
			break
	
	assert(provided_pickup)
	return provided_pickup

func advance_pickups_on_belt():
	
	for lane_index in get_lanes_indices():
		var lane = lanes[lane_index]
		var step = PICKUP_STEP / get_lane_ratio(lane)
		var size = PICKUP_SIZE
		var pickup_index = 0  # while instead of for because we remove elements
		while pickup_index < lane.order.size():
			var pickup = lane.order[pickup_index]
			var curr_progress = lane.data[pickup].progress
			var next_progress = min(curr_progress + step, 1.0)
			#var over_progress = curr_progress + step - next_progress
			var htbx_progress = next_progress + size * step
			
			assert(1 >= next_progress)
			
			# Optimisation flag to skip expensive confluence or next lane checks
			var do_deep_checks = (curr_progress >= lane.treshold)
#			var do_deep_checks = true
			
			# Set this to false to stop the pickup and increment its idle count
			var advance = true
			
			# Check, on own lane, the previous pickups in the lane queue
			# We need to check all of the previous because pickups might be
			# "piled" on top of one another at progress 0 when just fed,
			# when for example spawning a belt on top of pickups.
			if pickup_index > 0:
				for previous_pickup_index in range(pickup_index):
					var other_pickup = lane.order[previous_pickup_index]
					var other_progress = lane.data[other_pickup].progress
					#assert other_progress > curr_progress # many may start at 0
					if other_progress > curr_progress and other_progress < htbx_progress: # todo: try with <
						var diff_progress = htbx_progress - other_progress
						if diff_progress <= step:
							# Advance by a fraction of step
							next_progress = curr_progress + diff_progress
						else:
							advance = false
		#			if other_progress == curr_progress:
		#				continue
			
			assert(next_progress <= 1.0)
			
			# Check on confluent lanes
			# If another pickup is closer than us to the confluence, we stop.
			if do_deep_checks:
				var curr_remain = (1.0-curr_progress) * get_lane_ratio(lane)
				for confluent in lane.confluents:
					var other_lane = lanes[confluent]
					if other_lane.order.empty():
						continue
					var other_pickup = other_lane.order[0]
					var other_progress = other_lane.data[other_pickup].progress
					var other_remain = (1.0-other_progress) * get_lane_ratio(other_lane)
					
					if curr_remain > other_remain:
						advance = false
						break
			
			assert(next_progress <= 1.0)
			
			# Check on next lane
			if (pickup_index == 0) and (do_deep_checks) and (lane.has('next')):
				var next_lane = lanes[lane.next]
				if not is_lane_empty(next_lane):
					var mario = (curr_progress + PICKUP_SIZE * step) - 1.0
					if mario > 0:
						var luigi = mario * (get_lane_ratio(lane) / get_lane_ratio(next_lane))
						var yoshi = get_progress_of_last_on_lane(next_lane)
						if yoshi < luigi:
							var peach = luigi - yoshi
							if peach > PICKUP_STEP / get_lane_ratio(next_lane):
								advance = false
							else:
								var toad = peach * (get_lane_ratio(next_lane) / get_lane_ratio(lane))
								next_progress = curr_progress + toad
			
			
			# Check on the next belt
			if pickup_index == 0 and lane_index == 0:
				var next_belt = get_next_belt() # fails on opposite belts fixme
				if next_belt:
					var mario = (curr_progress + PICKUP_SIZE * step) - 1.0
					if mario > 0:
						var next_lane = next_belt.get_intake_lane_from(-1 * hex_direction)
						if not next_belt.is_lane_empty(next_lane):
							var luigi = mario * (get_lane_ratio(lane) / next_belt.get_lane_ratio(next_lane))
							var yoshi = next_belt.get_progress_of_last_on_lane(next_lane)
							if yoshi < luigi:
								var peach = luigi - yoshi
								if peach > PICKUP_STEP / next_belt.get_lane_ratio(next_lane): # who's to say that's correct? not me!
									advance = false
								else:
									var toad = peach * (next_belt.get_lane_ratio(next_lane) / get_lane_ratio(lane))
									next_progress = curr_progress + toad
			
			# We may remove items from the list we're iterating through
			var has_removed_pickup_from_lane = false
			
			if advance:
#				if not (0 == lane_index and 0 == pickup_index and 1.0 == next_progress):
#					print("Lane %d Pickup %d %f" % [lane_index, pickup_index, next_progress])
				
				# Check that no previous pickup in the lane queue is behind us
#				for jj in range(pickup_index):
#					assert lane.data[lane.order[jj]].progress >= next_progress
				
				lane.data[pickup].overflow = 0
				if next_progress >= 1.0:
					lane.data[pickup].overflow = curr_progress + step - next_progress
					assert(0 <= lane.data[pickup].overflow)
					next_progress = 1.0
				
				reposition_pickup(pickup, lane, next_progress)
				lane.data[pickup].progress = next_progress
				
#				assert next_progress <= 1.0
				if next_progress == 1.0:
					if lane.has('next'):
						assert(pickup_index == 0)
						var next_lane = lanes[lane.next]
						if can_feed_lane(next_lane):
							move_pickup_to_other_lane(lane, pickup, next_lane)
							has_removed_pickup_from_lane = true
					else:
						assert(0 == lane_index)
						assert(0 == pickup_index)
						if next_accepts_pickups():
							if should_move_to_next_belt(lane_index, pickup_index):
								move_to_next(lane_index, pickup_index)
				
			else:
				lane.data[pickup].idle = min(BIGINT, 1 + lane.data[pickup].idle)
			
			# … So we increment only when we did not remove any item
			if not has_removed_pickup_from_lane:
				pickup_index += 1

func reposition_pickup(pickup, lane, progress):
	pickup.position = lane.wp_start.linear_interpolate(
		lane.wp_end, progress
	)

func next_accepts_pickups():
	var next = get_next_belt()
	if next and next is get_script():
		var next_xy = god.board.hex_to_pix(hex_position + hex_direction * 0.5)
		if next.can_feed_pickup(null, next_xy):
			return true
	return false

func should_move_to_next_belt(lane_index, pickup_index):
	if not lane_index in LANES_TO_NEXT or pickup_index != 0:
		return false
	var pickup = lanes[lane_index].order[pickup_index]
	return lanes[lane_index].data[pickup].progress == 1.0

func move_to_next(lane_index, pickup_index):
	var next = get_next_belt()
	if next and next is get_script(): # Belt
		assert(next != self)  # Escher's nightmare
		var lane = lanes[lane_index]
		var pickup = lane.order[pickup_index]
		var next_lane = next.get_intake_lane_from(-1 * hex_direction)
		var overflow = lane.data[pickup].overflow * (get_lane_ratio(lane) / next.get_lane_ratio(next_lane))
		free_pickup(pickup)
		next.feed_pickup(pickup, pickup.position) # pickup progress is set to 0
		
		var next_belt_has_ticked = (self.current_machinery_tick == next.current_machinery_tick)
		if next_belt_has_ticked:
			next.apply_overflow_to_pickup(pickup, next_lane, overflow)


func apply_overflow_to_pickup(pickup, lane, overflow):
	
	# Pickup is not alone on the lane
	if lane.order.size() > 1: # more than me on this lane
		var pickup_index = lane.order.find(pickup)
		if pickup_index > 0:
			overflow = min(
				overflow,
				lane.data[lane.order[pickup_index-1]].progress \
				- \
				global_to_lane(lane, PICKUP_SIZE * PICKUP_STEP)
			)
	if overflow > 0:
		lane.data[pickup].progress = overflow
		reposition_pickup(pickup, lane, overflow) # <--- d'où ça
		
#		if lane.order.size() > 1: # more than me on this lane
#			var pickup_index = lane.order.find(pickup)
#			var progress = lane.data[pickup].progress
#			var previous_pickup = lane.order[pickup_index-1]
#			var previous_progress = lane.data[previous_pickup].progress
#			if previous_progress - progress > PICKUP_SIZE / get_lane_ratio(lane):
#				progress = previous_progress + PICKUP_STEP * get_lane_ratio(lane)
#				lane.data[pickup].progress = progress


func get_next_belt():
	"""
	Get the next belt, or null. Refactor me!
	"""
	var hex_next = self.hex_position + self.hex_direction
	var next_belt = god.board.get_belt(hex_next)
	if next_belt:
		if hex_position == next_belt.hex_position + next_belt.hex_direction:
			next_belt = null  # front-to-front belts ignore each other
	return next_belt


func recompute_waypoints():
	# IMPORTANT:
	# As the action do add new belts is right now, it will slurp
	# all pickups on its tile, including pickups of other belts.
	# We need to make sure that our pickups do not leave our tile, or fix
	# the issue elsewhere (Character, probably).
	# Some weeks later, I'm not entirely sure this is still the case…
	#papercut
	var half = 0.499
	###
	
	# Match letters to their point in pixel space
	var points = {
		'B': god.board.hex_to_pix(hex_position + hex_direction*half),
		'C': god.board.hex_to_pix(hex_position),
		'D': god.board.hex_to_pix(hex_position + hex_direction*half*self.yy),
	}
	var end_i = god.board.HEX_DIRECTIONS.find(hex_direction)
	for i in range(5):
		var other_dir = god.board.HEX_DIRECTIONS[(end_i+i+1)%6]
		points["I%d"%i] = god.board.hex_to_pix(hex_position + other_dir*half)
	
	# Actually recompute the waypoints from the letters
	for lane in lanes:
		lane.wp_start = points[lane.start]
		lane.wp_end = points[lane.end]


func reposition_sprite():
	var directions = god.board.HEX_DIRECTIONS
	var a = TAU / 6.0 # directions are separated by a sixth of the full turn
	var angles = [0*a, 1*a, 2*a, 3*a, 4*a, 5*a]
	assert(hex_direction in directions)
	rotation = angles[directions.find(hex_direction)]


func recompute_texture(do_neighbors_too=true):
	var s = ""
	var j = god.board.HEX_DIRECTIONS.find(hex_direction)
	for i in range(god.board.HEX_DIRECTIONS.size()):
		var d = god.board.HEX_DIRECTIONS[i]
		var k = (j + i) % 6
		var neighbor_hex = hex_position + god.board.HEX_DIRECTIONS[k]
		for neighbor in god.board.get_tile_fixtures(neighbor_hex):
			if neighbor is get_script(): # is Belt
				if do_neighbors_too:
					neighbor.recompute_texture(false)
				if ((
					(i == 0 and neighbor.hex_direction != self.hex_direction)
					or neighbor.get_next_belt() == self)):
					# fi
					s += "(%d,%d)" % [d.x, d.y]
					break
	
	set_texture(load("res://data/base/graphics/fixture/belt/belt_%s.png" % s))


### DEBUG ######################################################################

func get_dump():
	var pickups_desc = ""
	for i in get_lanes_indices():
		var lane_pickups = lanes[i].data
		if lane_pickups.size():
			pickups_desc += "Waypoint #%d %s\n" % [i, lanes[i].name]
			for pickup in lane_pickups:
				pickups_desc += "  %s at %f\n" % [pickup.name, lane_pickups[pickup].progress]
	return \
		"%s\n" % self.name + \
		"%d pickups\n%s\n" % [get_all_pickups().size(), pickups_desc]
#		"%s\n" % [lanes] # debug

func check_internal_statuses(): # everything looks ok now, remove this for prod
	pass
#	for pickup in pickups:
#		if null == pickup:
#			printerr("A pickup of %s is NULL !" % self.name)
#			assert pickup
#		if (not pickup) or (not pickup.is_inside_tree()):
#			printerr("Orphan %s %s in %s"%[pickup.name, pickup, self.name])
#			free_pickup(pickup)  # why are these still here in the first place?


#func recompute_intake(do_neighbors_too=false):
#	"""
#	For belts with at most one intake belt, we set here the to_prev
#	value and therefore the start of our linear interpolation to a cleverer
#	value than the default.
#	- it's recursive, and therefore out of the scope of one belt
#	- it's expensive (see line above)
#	- cow powers
#	"""
##	if not god:
##		assert _god  # used when called before `self` being added to the scene
##		god = _god
#	var belt = self
#	var on_tile = belt.hex_position
#	var to_prev = -1 * belt.to_next
#	var intakeBelts = []
#	for neighbor_tile in god.board.get_tiles_around(on_tile):
#		if neighbor_tile == on_tile + belt.to_next:
#			continue  # skip outake belt
#		var n_belt = god.board.get_belt(neighbor_tile)
#		if n_belt and neighbor_tile + n_belt.to_next == on_tile:
#			intakeBelts.append(n_belt)
#	if intakeBelts:
#		var intakeCount = intakeBelts.size()
#		if intakeCount == 1:
#			to_prev = intakeBelts[0].hex_position - on_tile
##		else:
##			print("Too many intakes for %s."%belt.name)
#
#	belt.set_to_prev(to_prev)
#
#	if do_neighbors_too:
#		for neighbor_tile in god.board.get_tiles_around(on_tile):
##			if neighbor_tile == ???:
##				continue  # skip parallel belts ?
#			var n_belt = god.board.get_belt(neighbor_tile)
#			if n_belt:
#				n_belt.recompute_intake(false)
