extends "res://script/core/Fixture.gd"

const Pickup = preload("res://script/core/Pickup.gd")
#const Utils = preload("res://script/lib/Utils.gd")

# Pickup currently handled
var pickup

var arm_sprite
var hand_sprite

# In pixel land
var _hand_position  # Vector2(x,y), private, use setter and getter
var hand_grab_radius = 32
var hand_grab_radius_squared = hand_grab_radius * hand_grab_radius

# Arm range, in hex tiles
var arm_range = 1

# Unit vectors in hex space
var to_intake = Vector2(-1, 0)  # left
var to_outake = Vector2( 1, 0)  # right

enum States {
	UNPOWERED,
	MOVING_TO_INTAKE,
	WAITING_FOR_INTAKE,
	MOVING_TO_OUTAKE,
	WAITING_FOR_OUTAKE,
}

var state = States.UNPOWERED

#onready var general_layer = god.get_general_layer()


func init(god, tile=Vector2(0,0), to_intake=Vector2(1,0), to_outake=null):
	"""
	Watch out for unpickle() as well if you edit the init() API signature.
	"""
	self.fixture_type = "inserter"
	self.god = god
	self.hex_position = tile
	self.hex_direction = to_intake
	self.to_intake = to_intake
	if not to_outake:
		to_outake = -1.0 * to_intake
	self.to_outake = to_outake
	
	state = States.MOVING_TO_INTAKE

func _ready():
	assert(god.board)
	
	# BODY
	set_texture(preload("res://data/base/graphics/fixture/inserter/inserter-body.png"))
	set_offset(Vector2(6,-4))
	set_hframes(2)
	var _scale = 0.500
	set_scale(Vector2(_scale, _scale))
	add_circular_collidable_body(0.2 / _scale)
	
	# ARM SPRITE
	arm_sprite = Sprite.new()
	arm_sprite.name = "InserterArm"
	arm_sprite.set_texture(preload("res://data/base/graphics/fixture/inserter/inserter-arm-ori.png"))
#	arm_sprite.set_scale(Vector2(1.0, 1.0)) # will be overwritten
#	arm_sprite.set_offset(Vector2(0, 0))
	add_child(arm_sprite)
	
	# HAND SPRITE
	hand_sprite = Sprite.new()
	hand_sprite.name = "InserterHand"
	hand_sprite.set_texture(preload("res://data/base/graphics/fixture/inserter/inserter-hand-ori.png"))
#	hand_sprite.set_scale(Vector2(1.0, 1.0)) # will be overwritten
#	hand_sprite.set_offset(Vector2(0, 0))
	add_child(hand_sprite)
	
	# ABSTRACT HAND
	# Order matters: texture of hand must exist already
	if null == get_hand_position():
		set_hand_position(get_resting_hand_position())
	
	# Last, because it also needs a hand position
	reorient_sprite()


### PICKLING ###################################################################

static func from_pickle(god, rick):
	var inserter = load("res://script/brick/fixture/Inserter.gd").new()
	inserter.init(
		god,
		rick.hex_position,
		rick.to_intake,
		rick.to_outake
	)
	inject_pickle(inserter, rick)
	if rick.has('hand_position'):
		inserter.set_hand_position(rick.hand_position, false)
	if rick.has('arm_range'):
		inserter.arm_range = rick.arm_range
	if rick.has('state'):
		inserter.state = rick.state
	if rick.has('pickup'):
		inserter.pickup = Pickup.from_pickle(god, rick.pickup)
		inserter.add_to_layer(inserter.pickup)
	
	return inserter

func to_pickle(relative_to=Vector2(0,0), for_blueprint=false):
	var pickle = Utils.merge(.to_pickle(relative_to, for_blueprint), {
		'to_intake': to_intake,
		'to_outake': to_outake,
		'arm_range': arm_range,
	})
	
	if not for_blueprint:
		pickle['state'] = state
		if pickup:
			pickle['pickup'] = pickup.to_pickle()
		if Vector2(0,0) == relative_to:
			pickle['hand_position'] = get_hand_position()
	
	return pickle


### ORIENTATION ################################################################

# Override parent method
func set_hex_direction(hex):
	self.hex_direction = hex
	self.to_intake = hex
	self.to_outake = -1.0 * hex
#	recompute_intake()
#	recompute_waypoints()
	recompute_hand_movement_prerequisites(true)
	reorient_sprite()
	wake_up()


### LOOP #######################################################################

var _currently_targeted_pickup_on_ground_wr
func on_machinery_tick(progress):
	if progress == 1.0 and OS.is_debug_build(): #benchmark which should go first
		check_internal_statuses()
	
	if state == States.UNPOWERED:
		return
	elif state == States.MOVING_TO_INTAKE:
		assert(not pickup)
		move_to_intake()
		if is_at_intake():
			state = States.WAITING_FOR_INTAKE
	elif state == States.WAITING_FOR_INTAKE:
		assert(not pickup)
		var ground_pickup
		var intake_pickup
		if _currently_targeted_pickup_on_ground_wr:
			var targeted_pickup = _currently_targeted_pickup_on_ground_wr.get_ref()
			if targeted_pickup:
				# Targeted pickup can't be too far away from the resting hand
				# position, or the inserter will chase pickups 4ever on belts.
				if (targeted_pickup.position - \
					get_resting_hand_position()).length_squared() < \
					hand_grab_radius_squared:
					# Since we're targeting a pickup that may have been grabbed
					# by another fixture, check if it's still on the ground.
					if god.board.is_pickup_on_grid(targeted_pickup):
						ground_pickup = targeted_pickup
					else:
						_currently_targeted_pickup_on_ground_wr = null
				else:
					# If the pickup is too far, stop targeting it.
					_currently_targeted_pickup_on_ground_wr = null
		if not ground_pickup:
			ground_pickup = get_pickup_on_ground_in_range_of_resting_hand()
		if ground_pickup:
			_currently_targeted_pickup_on_ground_wr = weakref(ground_pickup)
			move_to_grab(ground_pickup)
			if can_grab(ground_pickup):
				god.board.remove_from_grid_if_on_it(ground_pickup)
				grab(ground_pickup)
				assert(pickup)
				_currently_targeted_pickup_on_ground_wr = null
				state = States.MOVING_TO_OUTAKE
		else:
			for intake_fixture in get_intake_fixtures():
				var pickup_type = null
				var amount = 1
				if intake_fixture.can_provide_pickup(pickup_type, amount):
					if intake_fixture is god.fixtures.belt.script:
						intake_pickup = intake_fixture.get_one_pickup()
						move_to_grab(intake_pickup)  # for belts
						if can_grab(intake_pickup):
							intake_fixture.free_pickup(intake_pickup)
							grab(intake_pickup)
							assert(pickup)
							state = States.MOVING_TO_OUTAKE
							break
					else:
						intake_pickup = intake_fixture.provide_pickup(pickup_type, amount)
						grab(intake_pickup)
						assert(pickup)
						state = States.MOVING_TO_OUTAKE
						break
		
		if not (ground_pickup or intake_pickup or is_at_intake()):
			move_to_intake()
	
	elif state == States.MOVING_TO_OUTAKE:
		assert(pickup)
		assert(pickup != null)
		var wrp = weakref(pickup)
		assert(wrp.get_ref())
		move_to_outake()
		if is_at_outake():
			state = States.WAITING_FOR_OUTAKE
	elif state == States.WAITING_FOR_OUTAKE:
		assert(pickup)
		if can_drop():
			drop()
			state = States.MOVING_TO_INTAKE
#		else:  # for rotations
#			move_to_outake()
	else:
		assert(not "state not recognized")

func wake_up():
	if pickup:
		state = States.MOVING_TO_OUTAKE
	else:
		state = States.MOVING_TO_INTAKE


### STATUSES ###################################################################

const H_P_E = 6  # Hand Position Epsilon, squared

func has_pickup():
	return null != pickup

func is_at_intake():
	#glmatrix needed. (this feels inefficient)
	return (get_intake_position()-get_hand_position()).length_squared() < H_P_E

func is_at_outake():
	#glmatrix needed. (this feels inefficient)
	return (get_outake_position()-get_hand_position()).length_squared() < H_P_E

func get_intake_tile():
	return hex_position + to_intake * arm_range

func get_intake_position():
	return god.board.hex_to_pix(get_intake_tile()) #memoization

func get_outake_tile():
	return hex_position + to_outake * arm_range

func get_outake_position():
	return god.board.hex_to_pix(get_outake_tile()) #memoization

func get_intake_fixtures():
	return god.board.get_tile_fixtures(get_intake_tile())

func set_hand_position(xy, do_reposition_sprite=true):
	_hand_position = xy
	if do_reposition_sprite:
		reposition_hand_sprite(xy)

func get_hand_position(): # global
	return _hand_position

func get_resting_hand_position(): # global
	return get_intake_position()

#class NodeClosenessSorter:  # not used anymore, too slow, see below
#	var position
#	func _init(position):
#		self.position = position
#	func sort(a, b):
#		return (a.position-position).length_squared() < \
#		       (b.position-position).length_squared()

func get_pickup_on_ground_in_range_of_hand():
	var pickups = god.board.get_pickups_on_grid_in_circle(
		get_hand_position(), hand_grab_radius
	)
	if pickups:
		# we can shave sorting, for perfs ⋅⋅⋅ (wow, huge impact)
#		var sorter = NodeClosenessSorter.new(hand_position)
#		pickups.sort_custom(sorter, "sort")
		return pickups[0]
	return null

func get_pickup_on_ground_in_range_of_resting_hand():
	var pickups = god.board.get_pickups_on_grid_in_circle(
		get_resting_hand_position(), hand_grab_radius
	)
	if pickups:
		# we can shave sorting, for perfs ⋅⋅⋅ (wow, huge impact)
#		var sorter = NodeClosenessSorter.new(hand_position)
#		pickups.sort_custom(sorter, "sort")
		return pickups[0]
	return null

func get_all_pickups():
	if self.pickup:
		return [self.pickup]
	return Array()


### ACTIONS ####################################################################

#func free_pickup():
##	print("Freeing pickup %s from %s" % [pickup.name, self.name])
#	pickup = null

func move_to_intake():
	move_hand_to(get_intake_position())

func move_to_outake():
	move_hand_to(get_outake_position())

func move_to_grab(target_pickup):
	move_hand_to(target_pickup.position)

func move_hand_to(xy):
	var speed = 0.18
	if state == States.WAITING_FOR_INTAKE:
		speed = 0.31
	var next_position = get_hand_position().linear_interpolate(xy, speed)
	set_hand_position(next_position)

func can_grab(target_pickup):
	return (target_pickup.position - get_hand_position()).length_squared() < 5

func add_to_layer(pickup):
	god.add_to_general_layer(pickup)

func grab(target_pickup):
	assert(target_pickup)
	assert(not pickup)  # already has a pickup ?
	pickup = target_pickup
	pickup.pickable = false
	pickup.position = get_hand_position()
	add_to_layer(pickup)
	
#	emit_signal("pickup_grabbed", pickup,
#		god.board.pix_to_hex(pickup.position)
#	)

func can_drop():
#	prints(name, "CAN DROP?", god.board.can_drop(pickup, pickup.position))
	return god.board.can_drop(pickup, get_hand_position())

func drop():
	assert(pickup)
	pickup.pickable = true
#	prints("DrOPpiNg On BoArd", god.board.name, pickup.position)
	god.board.do_drop(pickup, get_hand_position())
	pickup = null


### PAINTING ###################################################################

const LIFE_UNIVERSE_EVERYTHING = 42
const DEATH_VOID_NOTHING = -42

#var texture_size
var shoulder_global
# In texture space ; don't provide Y (it's not supported yet)
var hand_local_start = Vector2(15, 0)
var hand_local_end = Vector2(46, 0)
var arm_local_start = Vector2(22, 0)
var arm_local_end = Vector2(102, 0)

var hand_prerequisites_stale = true
var hex_direction_angle = 0

#func set_texture(txtr):
#	hand_prerequisites_stale = true
#	.set_texture(txtr)

func recompute_hand_movement_prerequisites(force=false):
	if hand_prerequisites_stale or force:
		hand_prerequisites_stale = false
#		texture_size = hand_sprite.get_texture().get_size()
#		hand_local_start = Vector2(0.3, 0) * texture_size
#		hand_local_end = Vector2(0.73, 0) * texture_size
		shoulder_global = get_global_transform() \
			.xform(Vector2(0, DEATH_VOID_NOTHING)) # coincidence? I THINK NOT!
		var i = god.board.HEX_DIRECTIONS.find(to_intake)
		assert(i > -1)  # to_outake in directions (cheaper, though harder to read)
		hex_direction_angle = i * TAU / 6.0

func reposition_hand_sprite(xy):
	recompute_hand_movement_prerequisites()
	
	var hand_local = get_global_transform().xform_inv(xy)
	var hand_local_rotated = hand_local.rotated(-hex_direction_angle)
	
#	var is_mirrored = xy.x > shoulder_global.x
	var is_mirrored = 0 < hand_local.x
	var riri = - TAU / 12.0
	var fifi = - TAU / 256.0
	var loulou = riri - fifi * 1.618 * abs(hand_local_rotated.y)
	if is_mirrored:
		loulou *= -1
	var s2h = shoulder_global - xy
	var elbow_global = xy + s2h.rotated(loulou).normalized() * 15.0
	
	var arm_global_start = shoulder_global + \
			5 * (elbow_global - shoulder_global).normalized()
	
	Utils.reposition_sprite_with_flip_between(
		hand_sprite, 0.60, is_mirrored,
		hand_local_start, hand_local_end,
		elbow_global, xy
	)
	
	Utils.reposition_sprite_with_flip_between(
		arm_sprite, 0.01 * LIFE_UNIVERSE_EVERYTHING, is_mirrored,
		arm_local_start, arm_local_end,
		arm_global_start, elbow_global
	)
	
	# Thicken or thinnen the arm to make it more lively
#	var arm_scale = arm_sprite.get_scale()
#	var hys = max(0.9, min(1.0, 1.0 / (arm_scale.x)))
#	arm_sprite.set_scale(Vector2(arm_scale.x, hys))
	
	if pickup:
		pickup.position = xy

#func reorient_hand_sprite(xy):
#	if hand_sprite.position.x < 0:
#		hand_sprite.flip_h = true
#	else:
#		hand_sprite.flip_h = false

func reorient_sprite():
	var directions = god.board.HEX_DIRECTIONS
	var i = directions.find(to_intake)
	assert(i > -1)  # to_outake in directions (cheaper, though harder to read)
	
	if i % 2 == 0:
		self.frame = 0 #Left
	else:
		self.frame = 1 #Right
	
	reposition_hand_sprite(get_hand_position())


### DEBUG ######################################################################

func get_dump():
	var desc = "%s\n" % self.name
	if pickup:
		desc += "  carries %s (%s)\n" % [pickup.name, pickup]
	desc += "  from %s to %s\n" % [get_intake_tile(), get_outake_tile()]
	desc += "  hand is at %s\n" % get_hand_position()
	desc += "  state is %s\n" % state
	desc += "Pickled :\n%s\n" % to_pickle()
	return desc

func check_internal_statuses():
	if pickup and not pickup.is_inside_tree():
		printerr("Found orphan %s %s in %s"%[pickup.name, pickup, self.name])
		breakpoint

