extends "res://script/core/Fixture.gd"

const Inventory = preload("res://script/core/Inventory.gd")

# Spawned pickup type
var pickup_type = "pouloupinium"

# Pickups spawned, waiting to be dropped
var inventory

# At 60 ticks per second, how many seconds between spawns
var spawn_time = 1.618
var spawn_speed = 1.0 / (60.0 * self.spawn_time)
# PITFALL : spawn_time here without self has the _init parameter value
#var spawn_speed = 1.0 / (60.0 * spawn_time)

# Unit vector in hex space
var to_outake = Vector2(1, 0)  # right

# Indices of frames to display sequentially when animating
const FRAMES_FILM = [0,0,1,1,2,2,3,3,4,4,3,3,2,2,1,1]
const FRAMES_FILM_LENGTH = 16 # size of above (for perfs)

# Array of (flipH, angle) for each direction clockwise from 3 o'clock
# angle is in radians, also clockwise from 3 o'clock
const SPRITE_META = [
	Vector2(0, 0), Vector2(0, TAU/6.0), Vector2(1, -TAU/6.0),
	Vector2(1, 0), Vector2(1, TAU/6.0), Vector2(0, -TAU/6.0)
]

const STATE_UNPOWERED = 0
const STATE_SPAWNING = 1
const STATE_ACTIVELY_WAITING_FOR_OUTAKE = 2
const STATE_LAZILY_WAITING_FOR_OUTAKE = 3

# Useful for debug
#const STATE_UNPOWERED = 'STATE_UNPOWERED'
#const STATE_SPAWNING = 'STATE_SPAWNING'
#const STATE_ACTIVELY_WAITING_FOR_OUTAKE = 'STATE_ACTIVELY_WAITING_FOR_OUTAKE'
#const STATE_LAZILY_WAITING_FOR_OUTAKE = 'STATE_LAZILY_WAITING_FOR_OUTAKE'


var state = STATE_UNPOWERED


func init(god, tile=Vector2(0,0), direction=Vector2(1,0),
		   pickup_type=null, spawn_time=null):
	"""
	Watch out for unpickle() as well if you edit the _init() API signature.
	"""
	.init(god, tile, direction)
	self.fixture_type = "pickup_spawner"
	if pickup_type: # else pouloupinium is the default
		self.pickup_type = pickup_type
	if null != spawn_time:
		assert(spawn_time >= 0)  # maybe negative values work. Still... No.
		self.spawn_time = spawn_time
		self.spawn_speed = 1.0 / (60.0 * self.spawn_time)
	self.to_outake = direction
	self.inventory = Inventory.new(1, 1, false, pickup_type)

func _ready():
	state = STATE_SPAWNING
	
	var texture_path = "res://data/base/graphics/fixture/pickup-spawner/" + \
					   "%s-spawner-spritesheet.png" # tabs&spaces (╯°□°）╯︵ ┻━┻
	
	# The corresponding files should exist.
	var customized_spawners = [
		'calcium',
		'carbon',
		'copper',
		'chlorine',
		'erythrocyte',
		'hydrogen',
		'iodine',
		'iron',
		'manganese',
		'magnesium',
		'nitrogen',
		'oxygen',
		'phosphorus',
		'potassium',
		'sodium',
		'sulfur',
		'egg',
	]
	
	# Instead of maintaining the above customized_spawners, we tried different
	# methods to try to detect if the resource exists.
	# A. load() raises errors when the file does not exist. No.
	#    ResourceLoader.load() yields the same.
	# B. ResourceLoader.has() is only true after a successful load.
	# C. File.new().file_exists() is false in distributed binaries for res:// ?
	#    But it works in Godot
	# Help would be appreciated.
	
	if self.pickup_type in customized_spawners:
		texture_path = texture_path % self.pickup_type
	else:
		texture_path = texture_path % "carbon" # default
	
	set_texture(load(texture_path))
	set_hframes(5)
	set_offset(Vector2(0,-6))
	
	var _scale = 1.0
	set_scale(Vector2(_scale, _scale))
	add_circular_collidable_body(0.2 / _scale)
	
	reorient_sprite()


### PICKLING ###################################################################

static func from_pickle(god, rick):
	var spawner = load("res://script/brick/fixture/PickupSpawner.gd").new()
	spawner.init(
		god,
		rick.hex_position,
		rick.to_outake,
		rick.pickup_type,
		rick.spawn_time
	)
	
	inject_pickle(spawner, rick)
	
	if rick.has('inventory'):
		spawner.inventory = Inventory.from_pickle(god, rick.inventory)
	
	return spawner

func to_pickle(relative_to=Vector2(0,0), for_blueprint=false):
	var pickle = Utils.merge(.to_pickle(relative_to, for_blueprint), {
		'pickup_type':   pickup_type,
		'spawn_time':    spawn_time,
		'to_outake':     to_outake,
	})
	
	if not for_blueprint:
		pickle['inventory'] = inventory.to_pickle()
	
	return pickle


### DIRECTION ##################################################################

# Override parent setget method
func set_hex_direction(hex):
	self.hex_direction = hex
	self.to_outake = hex
	reorient_sprite()


### LOOP #######################################################################

#var spawn_speed = 1.0 / (60.0 * spawn_time)
var spawn_progress = 0.0
var ticks_working_for_nothing = 0
var cursor_idling_when_lazy = 0  # in ticks
var length_idling_when_lazy = 99 # total
func on_machinery_tick(progress):
	if progress == 1.0 and OS.is_debug_build(): #benchmark which should go first
		check_internal_statuses()

	if state == STATE_UNPOWERED:
		return
	elif state == STATE_SPAWNING:
		animate_sprite()
		if is_full():
			state = STATE_ACTIVELY_WAITING_FOR_OUTAKE
		elif spawn_progress >= spawn_time:
			spawn_progress = 0
			var spawned_pickup = god.spawn_pickup(pickup_type)
			add_to_inventory(spawned_pickup)
			if can_drop():
				do_drop()
			# add this if we don't want to wait for one more tick ?
#			if is_full():
#				state = STATE_WAITING_FOR_OUTAKE
		else:
			spawn_progress += spawn_speed
			if can_drop():
				do_drop()
	elif state == STATE_ACTIVELY_WAITING_FOR_OUTAKE:
		if can_drop():
			do_drop()
			state = STATE_SPAWNING
			ticks_working_for_nothing = 0
		else:
			ticks_working_for_nothing += 1
		
		if ticks_working_for_nothing > 63:  #benchmark "> 63" vs "> 64"
			ticks_working_for_nothing %= 1048576  # avoid buffer overflow :]
			state = STATE_LAZILY_WAITING_FOR_OUTAKE

	elif state == STATE_LAZILY_WAITING_FOR_OUTAKE:
		if cursor_idling_when_lazy < length_idling_when_lazy:
			cursor_idling_when_lazy += 1  # least possible amount of calculus
		else:
			cursor_idling_when_lazy = 0
			state = STATE_ACTIVELY_WAITING_FOR_OUTAKE
	else:
		printerr("State '%s' of %s not recognized." % [state, self.name])
		assert(not "supported")


### STATUSES ###################################################################

func is_full():
	return inventory.is_full()

func add_to_inventory(pickup):
	inventory.store(pickup)

func can_drop():
#	assert inventory
	if inventory.is_empty():
		return false
	var pickup = inventory.find()
	if god.board.can_drop(pickup, get_outake_position()):
		return true
	else:
		return false

func do_drop():
#	assert inventory
	var pickup = inventory.retrieve()
	god.board.do_drop(pickup, get_outake_position())

func get_outake_position():
	return god.board.hex_to_pix(hex_position + 0.75 * to_outake) #memoization


### ACTIONS ####################################################################

#func free_pickup(pickup):
##	print("Freeing pickup %s from %s" % [pickup.name, self.name])
#	assert pickup in inventory
#	inventory.remove(inventory.find(pickup))


### PAINTING ###################################################################

var _current_frame = 0

func animate_sprite():
	frame = FRAMES_FILM[_current_frame]
	_current_frame = (_current_frame + 1) % FRAMES_FILM_LENGTH

func reorient_sprite():
	var i = god.board.HEX_DIRECTIONS.find(to_outake)
	assert(i > -1)
	set_flip_h(SPRITE_META[i][0])
	set_rotation(SPRITE_META[i][1])


### DEBUG ######################################################################

func get_dump():
	var desc = "%s\n" % self.name
	if not inventory.is_empty():
		desc += "  holds %s\n" % [inventory.to_string()]
	desc += "  at %s looking to %s\n" % [hex_position, to_outake]
	desc += "  state is %s\n" % state
	desc += "Pickled :\n%s\n" % to_pickle()
	return desc

func check_internal_statuses():
	pass
#	if pickup and not pickup.is_inside_tree():
#		print("Found orphan %s %s in %s"%[pickup.name, pickup, self.name])
#		free_pickup()

