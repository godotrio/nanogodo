extends "res://script/core/Fixture.gd"

################################################################################

# Any pickup provided to the despawner will be despawned and its stack will
# be added to the internal log of despawned pickups.

################################################################################

const Inventory = preload("res://script/core/Inventory.gd")

const FRAMES_ORDER = [1,2,3,4,5,6,7,8,7,6,5,4,3,2,1,0] #carbon spawner spritesheet
var FRAMES_ORDER_LENGTH = FRAMES_ORDER.size() # size of, above, for perfs

# Only pickups of this type will be allowed.
# When set to null, all pickups are allowed.
var pickup_type = null # or String

# We keep track of how many pickups we despawned.
var despawned_count = Dictionary() # pickup_type => Integer


func init(god, tile=Vector2(0,0), direction=Vector2(1,0), pickup_type=null):
	"""
	Watch out for from_pickle() as well if you edit the _init() API signature.
	"""
	.init(god, tile, direction)
	self.fixture_type = "pickup_despawner"
	if pickup_type:
		self.pickup_type = pickup_type

func _ready():
	assert(god.board)
	
	set_texture(preload("res://data/base/graphics/fixture/pickup-spawner/erythrocyte-despawner-spritesheet.png"))
	set_hframes(9)
#	set_vframes(4)
	#set_centered(true) # the default is true
	set_offset(Vector2(3, -7))
	var _scale = 0.55
	set_scale(Vector2(_scale, _scale))
	add_circular_collidable_body(0.55 / _scale)


### PICKLING ###################################################################

static func from_pickle(god, rick):
	var despawner = load("res://script/brick/fixture/PickupDespawner.gd").new()
	despawner.init(
		god,
		rick.hex_position,
		rick.hex_direction,
		rick.pickup_type
	)
	
	inject_pickle(despawner, rick)
	
	if rick.has('despawned_count'):
		despawner.despawned_count = rick.despawned_count
	
	return despawner

func to_pickle(relative_to=Vector2(0,0), for_blueprint=false):
	var pickle = Utils.merge(.to_pickle(relative_to, for_blueprint), {
		'pickup_type': pickup_type,
	})
	
	if not for_blueprint:
		pickle['despawned_count'] = despawned_count
	
	return pickle


## LOOP ########################################################################

func on_machinery_tick(progress):
	animate_sprite()


### INTAKE INTERFACE ###########################################################

func can_feed_pickup(pickup, xy):
	"""
	Whether this fixture can be fed the pickup at position xy.
	"""
	return true

func feed_pickup(pickup, xy):
	if not self.despawned_count.has(pickup.pickup_type):
		self.despawned_count[pickup.pickup_type] = 0
	self.despawned_count[pickup.pickup_type] += pickup.stack
	god.despawn_pickup(pickup)


### OUTAKE INTERFACE ###########################################################

func can_provide_pickup(pickup_type=null, amount=1):
	return false

func provide_pickup(pickup_type=null, amount=1):
	"""
	Must:
	- return a Pickup instance (that may never have been added to the scene tree)
	- remove the pickup from this fixture's internal memory
	"""
	printerr("Despawners cannot provide pickups! ABORT! ABORT!")
	assert(not "allowed")


### PAINTING ###################################################################

var _current_frame = 0
var _current_vframe = 0

func animate_sprite(use_vframe=null):
	frame = _current_vframe * 8 + FRAMES_ORDER[_current_frame]
	_current_frame = (_current_frame + 1) % FRAMES_ORDER_LENGTH


### DEBUG ######################################################################

func get_dump():
	var desc = "Despawner %s\n" % self.name
	desc += "Pickled :\n%s\n" % to_pickle()
	return desc
