extends "res://script/brick/pickup/OrganicPickup.gd"

# Approximately 2.4 million new erythrocytes (red cells) are produced per second
# These cells have an average volume of about 90 fL (femtoLiters)
# with a surface of about 136 μm2,
# and can swell up to a sphere shape containing 150 fL.
