extends "res://script/core/Pickup.gd"

# Water is a core component of all life as we know it, except for robots.


func init(god, config, stack, entropy=0):
	.init(god, config, stack, entropy)
	set_scale(Vector2(0.35,0.35))


### PAINTING ###################################################################

const SPRITESHEET_HFRAMES = 3

func _enter_tree():
	set_texture(preload("res://data/base/graphics/pickup/blood/blood-spritesheet.png"))
	set_hframes(SPRITESHEET_HFRAMES)
	for i in range(SPRITESHEET_HFRAMES):
		set_frame(i)
		for j in range(42):
			yield(get_tree(), "idle_frame")

func _exit_tree():
	set_texture(preload("res://data/base/graphics/pickup/water/water.png"))
	set_frame(0)
