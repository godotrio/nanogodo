extends "res://script/core/Pickup.gd"

#Symbol: O
#About 65% of the total weight of the body

func init(god, config, stack, entropy=0):
	.init(god, config, stack, entropy)
	rotation_degrees = (randi() % 6) * 60
