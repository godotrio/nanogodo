extends "res://script/core/Pickup.gd"

#Symbol: C
#About 18% of the total weight of the body

func init(god, config, stack, entropy=0):
	.init(god, config, stack, entropy)
	rotation_degrees = ((randi() % 3) - 1) * 30
