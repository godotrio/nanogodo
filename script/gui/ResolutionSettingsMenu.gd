extends Control
var resolution_list = {
0: { "name": "800x600 (4:3)", "vector": Vector2(800,600) },
1: { "name": "1024x600 (17:10)", "vector": Vector2(1024,600) },
2: { "name": "1024x768 (4:3)", "vector": Vector2(1024,768) },
3: { "name": "1152x864 (4:3)", "vector": Vector2(1152,864) },
4: { "name": "1280x720 (16:9)", "vector": Vector2(1280,720) },
5: { "name": "1280x768 (5:3)", "vector": Vector2(1280,768) },
6: { "name": "1280x800 (16:10)", "vector": Vector2(1280,800) },
7: { "name": "1280x1024 (5:4)", "vector": Vector2(1280,1024) },
8: { "name": "1360x768 (16:9)", "vector": Vector2(1360,768) },
9: { "name": "1366x768 (16:9)", "vector": Vector2(1366,768) },
10: { "name": "1440x900 (16:10)", "vector": Vector2(1440,900) },
11: { "name": "1536x864 (16:9)", "vector": Vector2(1536,864) },
12: { "name": "1600x900 (16:9)", "vector": Vector2(1600,900) },
13: { "name": "1680x1050 (16:10)", "vector": Vector2(1680,1050) },
14: { "name": "1920x1080 (16:9)", "vector": Vector2(1920,1080) },
15: { "name": "1920x1200 (16:10)", "vector": Vector2(1920,1200) },
16: { "name": "2560x1080 (21:9)", "vector": Vector2(2560,1080) },
17: { "name": "2560x1440 (16:9)", "vector": Vector2(2560,1440) },
18: { "name": "3440x1440 (21:9)", "vector": Vector2(3440,1440) },
19: { "name": "3840x2160 (16:9)", "vector": Vector2(3840,2160) }
}



const buttons_name = [ "APPLY", "CANCEL" ]
var index_limit
var buttons_group
onready var buttons_parent = $BackgroundContainer/MarginContainer/VBoxContainer/MarginContainer/ScrollContainer/MarginContainer/VBoxContainer/VBoxContainer
onready var apply_button = get_node("BackgroundContainer/MarginContainer/VBoxContainer/MarginContainer/ScrollContainer/MarginContainer/VBoxContainer/ApplyButton")
onready var cancel_button = get_node("BackgroundContainer/MarginContainer/VBoxContainer/MarginContainer/ScrollContainer/MarginContainer/VBoxContainer/CancelButton")
onready var group

var menu_node
func set_menu_node(menu_node):
	self.menu_node = menu_node

var pressed_node


var __resolution 
var __vsync = false
var __fullscreen = false
var __borderless = false
var __resizable = false



func _ready():
	__resolution = OS.window_size
#	__vsync = menu_node.config_dict.Video.vsync
#	__fullscreen = menu_node.config_dict.Video.fullscreen
##	__borderless = get_parent().config_dict.Video.borderless
#	__resizable = menu_node.config_dict.Video.resizable
	
	for i in range(0,19):
		
		if resolution_list[i].vector.x <= OS.get_screen_size().x && resolution_list[i].vector.y <= OS.get_screen_size().y:
			var node = preload("res://scene/CheckBoxForButtonGroup.tscn").instance()
			node.toggle_mode = true
			node.connect("pressed", self, "check_button_pressed", [node])
			node.set_text(resolution_list[i].name)
			resolution_list[i]["node"] = node
			buttons_parent.add_child(node)
			buttons_group = node.group
	
	apply_button.connect("pressed", self, "apply_action")
	cancel_button.connect("pressed", self, "cancel_action")

	apply_button.set_text(buttons_name[0])
	cancel_button.set_text(buttons_name[1])


func check_button_pressed(node):
	for i in range(0,19):
		if resolution_list[i].has("node"):
			if resolution_list[i].node != node:
				resolution_list[i].node.pressed = false
			else:
				__resolution = resolution_list[i]["vector"]
				print(__resolution)
	pressed_node = node
	print(pressed_node)
		
		
			
		
	

func apply_action():
	print("resolution = %s" % __resolution)
	if menu_node.name == "Menu":
		menu_node.set_screen_settings_and_confirm(__resolution, __fullscreen, true, __resizable, __vsync)
	else:
		OS.alert( "Please load a parent that can apply settings (Menu.tscn)", "Parent scene doesn't hold this method" )


func cancel_action():
	menu_node.load_video_settings_menu()
	pass
