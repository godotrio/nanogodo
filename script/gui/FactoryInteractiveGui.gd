#extends "res://script/gui/WindowBasedContainer.gd"
extends CenterContainer

const InteractiveInventory = preload("res://script/gui/InteractiveInventoryGui.gd")
const RecipeSelectionGui   = preload("res://script/gui/RecipeSelectionGui.gd")
const CLOSE_BUTTON_NORMAL  = preload("res://data/base/graphics/ui/panel_cross_stylebox_normal.tres")
const CLOSE_BUTTON_HOVER   = preload("res://data/base/graphics/ui/panel_cross_stylebox_hover.tres")
const CLOSE_BUTTON_PRESSED = preload("res://data/base/graphics/ui/panel_cross_stylebox_pressed.tres")

var recipe_gui
var vbox
var hbox_intake
var hbox_outake
var background

var in_iuis = Array()
var out_iuis = Array()
var progress_bar

var god
var fixture
var character
var hand

var padding = 5

func _init(god, fixture, title, available_recipes,
		   intake_inventories, outake_inventories):
	assert(god)
	assert(god.game)
	assert(god.game.character)
	self.god = god
	self.fixture = fixture
	self.character = god.game.character
	self.hand = self.character.hand
	
#	self.mfv_anchor_top		= 0.5
#	self.mfv_anchor_bottom	= 0.5
#	self.mfv_anchor_left	= 0.5
#	self.mfv_anchor_right	= 0.5

	self.theme = preload("res://data/base/graphics/ui/hud_theme.tres")

	self.anchor_top = 0
	self.anchor_left = 0
	self.anchor_right = 1
	self.anchor_bottom = 1
	self.margin_top = -100
#	self.mouse_filter = MOUSE_FILTER_IGNORE
	
	recipe_gui = RecipeSelectionGui.new(god, available_recipes, null, true)
	recipe_gui.connect("slot_pressed", self, "on_recipe_slot_pressed", [])
	#var recipe_gui_size = recipe_gui.rect_min_size  # we could also use this
	
	hbox_intake = HBoxContainer.new()
	hbox_outake = HBoxContainer.new()
	vbox = VBoxContainer.new()
	progress_bar = ProgressBar.new()
	
	update_iotakes(intake_inventories, outake_inventories)
	
	background = Panel.new()
	background.anchor_top = 0
	background.anchor_left = 0
	background.anchor_right = 1
	background.anchor_bottom = 1
	background.margin_top = 0
	background.margin_left = 0
	background.margin_right = 0
	background.margin_bottom = 0
	background.show_behind_parent = true
	
	add_child(background)
	
#	var header = Container.new()
#	background.anchor_top = 0
#	background.anchor_left = 0
#	background.anchor_right = 1
#	background.anchor_bottom = 1
	
	vbox.add_child(hbox_intake)
	vbox.add_child(recipe_gui)
	vbox.add_child(progress_bar)
	vbox.add_child(hbox_outake)

	add_child(vbox)
	
	update_display()

#func _ready():
#	print(vbox.rect_size)  # it's ok here

func on_recipe_slot_pressed(slot_node, recipe_slug, action_type):
	unhook_hand()
	self.character.hand.clear(false)
	assert(self.character.hand.is_empty())
	
	if self.fixture.get_recipe_type() == recipe_slug:
		return  # skip
	
	var pickups = self.fixture.get_all_pickups()
	
	if self.character.has_room_for_pickups(pickups):
		self.character.store_all(pickups)
		for slot in self.recipe_gui.slots.values():
			if slot == slot_node:
				if not slot.pressed:
					self.fixture.forget_recipe_type()
				else:
					self.fixture.set_recipe_type(recipe_slug)
#					slot.pressed = true
				self.recipe_gui.update_display()
			else:
				slot.pressed = false
	update_recipe_slots()


func on_close_pressed():
	fixture.close_interactive_ui()


func _gui_input(event):
	if event is InputEventMouseButton:
		fixture.close_interactive_ui()


func close():
	get_parent().remove_child(self)
	self.queue_free()


func update_iotakes(intake_inventories, outake_inventories):
	for child in hbox_intake.get_children():
		hbox_intake.remove_child(child)
		child.queue_free()
	for child in hbox_outake.get_children():
		hbox_outake.remove_child(child)
		child.queue_free()
	var i = 0
	in_iuis = Array()
	for intake_inventory in intake_inventories:
		in_iuis.append(InteractiveInventory.new(god, self.character, intake_inventory))
		hbox_intake.add_child(in_iuis[i])
		i += 1
	i = 0 
	out_iuis = Array()
	for outake_inventory in outake_inventories:
		out_iuis.append(InteractiveInventory.new(god, self.character, outake_inventory))
		hbox_outake.add_child(out_iuis[i])
		i += 1


## ACTIONS #####################################################################

func unhook_hand():
	for iui in self.in_iuis:
		if iui.inventory == self.hand.held_inventory:
			self.hand.clear()

func update_display():
	for in_iui in self.in_iuis:
		in_iui.update_display()
	
	for out_iui in self.out_iuis:
		out_iui.update_display()
	
	#print(fixture.get_craft_progress())
	progress_bar.value = self.fixture.get_craft_progress()*100
	self.recipe_gui.update_display(null)
	
	update_recipe_slots()
	update_background()


func update_recipe_slots():
	assert(self.recipe_gui)
	for slot in self.recipe_gui.slots.values():
#		if self.fixture.get_active_recipe_slug() == slot.recipe_slug:  #naming
		if self.fixture.get_recipe_type() != slot.recipe_slug:
			slot.pressed = false
		else:
			slot.pressed = true


func update_background():
	remove_child(background)
	background.rect_min_size = get_minimum_size() + Vector2(2*padding,2*padding)
	add_child(background)
	move_child(background, 0)
