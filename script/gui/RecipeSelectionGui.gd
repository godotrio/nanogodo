extends Control

const RecipeSelectionSlot = preload("RecipeSelectionSlot.gd")

const SLOTS_LINES_CONTAINER_NODE_NAME = "SlotsLinesContainer"
const SLOTS_LINE_NODE_NAME = "SlotsLine%d"
const SLOT_NODE_NAME = "Slot%d"
const SLOT_LABEL_NODE_NAME = "SlotLabel"
const SLOT_ICON_NODE_NAME = "SlotIcon"


var god
var character
var available_recipes
var recipe_labels
var mode_toggle

# Slots coordinates start at 1, not 0.
var slots_size_x
var slots_size_y
var slots = Dictionary() # Vector2 => RecipeSelectionSlot

func _init(god, available_recipes, recipe_labels=null, mode_toggle=false):
	self.god = god
	self.character = god.game.character
	self.mode_toggle = mode_toggle
	
	if null == recipe_labels:
		self.recipe_labels = Dictionary()
	else:
		self.recipe_labels = recipe_labels
	
	assert(available_recipes)
	self.available_recipes = available_recipes
	
	self.slots_size_x = self.available_recipes.size()
	self.slots_size_y = 1
	
	create_tree_children()


func create_tree_children():
	assert(get_child_count() == 0)
	
	var slot_rect_side = 30 # px
	var separator_x = 49
	var slot_x_padding = 4
	var slot_y_padding = 4
	var x_padding = 5
	var y_padding = 5
	
#	mfv_anchor_left   = 0.5
#	mfv_anchor_top    = 1.0
#	mfv_anchor_right  = 0.5
#	mfv_anchor_bottom = 1.0
	
	var background = NinePatchRect.new()
	background.name = "JE_SUIS_MIELPOPS"
	background.texture = preload("res://data/base/graphics/ui/panel.png")
	background.axis_stretch_horizontal = NinePatchRect.AXIS_STRETCH_MODE_TILE_FIT
	background.axis_stretch_vertical = NinePatchRect.AXIS_STRETCH_MODE_TILE_FIT
	background.patch_margin_left   = 5
	background.patch_margin_top    = 5
	background.patch_margin_right  = 5
	background.patch_margin_bottom = 5
	background.anchor_left   = 0
	background.anchor_top    = 0
	background.anchor_right  = 1
	background.anchor_bottom = 1
	
	add_child(background)
	
	var lines_container = VBoxContainer.new()
	lines_container.name = SLOTS_LINES_CONTAINER_NODE_NAME
	lines_container.anchor_left   = 0
	lines_container.anchor_top    = 0
	lines_container.anchor_right  = 1
	lines_container.anchor_bottom = 1
	lines_container.rect_position = Vector2(5, 5) # leave room for background
	
	for y in range(1, self.slots_size_y + 1):
		
		var line = HBoxContainer.new()
		line.name = SLOTS_LINE_NODE_NAME % y
		var i = 0
		var recipes_slugs = available_recipes.keys()
		for x in range(1, self.slots_size_x + 1):
			var recipe_slug = null
			if recipes_slugs.size() > i:
				recipe_slug = recipes_slugs[i]
			assert(recipe_slug)
			var slot_key = Vector2(x, y)
			var slot
			
			slot = RecipeSelectionSlot.new(self, recipe_slug, self.mode_toggle)
			
#			if ui_parent.fixture.get_recipe_type() == recipe_slug:
#				slot.pressed = true
			
			slot.name = SLOT_NODE_NAME % x
			slot.theme = preload("res://data/base/graphics/ui/toolbelt_slot_style.tres")
#			slot.rect_position = Vector2(-1,-1)
			slot.rect_min_size = Vector2(slot_rect_side, slot_rect_side)
			slot.rect_clip_content = true
			
			# We use a Sprite and not a TextureRect, to be able to animate it.
			# Holds the pickup texture or the hand (animated) texture or nothing
			var slot_img = Sprite.new()
			slot_img.name = SLOT_ICON_NODE_NAME
			slot_img.offset = Vector2(-1, -1)
			slot_img.centered = false
			slot.add_child(slot_img)
			
			var slot_lbl = Label.new()
			slot_lbl.name = SLOT_LABEL_NODE_NAME
			slot_lbl.grow_vertical = Control.GROW_DIRECTION_BEGIN
			slot_lbl.grow_horizontal = Control.GROW_DIRECTION_BEGIN
			slot_lbl.align = Label.ALIGN_RIGHT
			slot_lbl.anchor_top = 0.97
			slot_lbl.anchor_left = 0.96
			slot.add_child(slot_lbl)
			
			line.add_child(slot)
			slots[slot_key] = slot
			i += 1
		lines_container.add_child(line)
	
	add_child(lines_container)
	rect_min_size = Vector2(
		slots_size_x * (slot_rect_side + slot_x_padding) + x_padding,
		slots_size_y * (slot_rect_side + slot_y_padding) + y_padding
	)


## ACTIONS #####################################################################

signal slot_pressed(slot_node, recipe_slug, action_type)

# This method is nominatively called by the slots
func on_slot_pressed(slot_node, recipe_slug, action_type):
	emit_signal("slot_pressed", slot_node, recipe_slug, action_type)


### DISPLAY ####################################################################

func update_display(recipe_labels=null):
	# 1. Update this crafting UI
	var skip_labels = false
	if not recipe_labels:
		skip_labels = true
	else:
		self.recipe_labels = recipe_labels
	var i = 0
	var recipes_slugs = available_recipes.keys()
	for slot_key in slots:
		var slot = slots[slot_key]
		assert(slot.has_node(SLOT_ICON_NODE_NAME))
		var slot_img = slot.get_node(SLOT_ICON_NODE_NAME)
		assert(slot.has_node(SLOT_LABEL_NODE_NAME))
		var slot_lbl = slot.get_node(SLOT_LABEL_NODE_NAME)
		var recipe_slug = null
		var recipe = null
		if recipes_slugs.size() > i:
			recipe_slug = recipes_slugs[i]
			recipe = available_recipes[recipe_slug]
		
		if recipe:
			if not skip_labels:
				var my_label = ""
				if self.recipe_labels.has(recipe.slug):
					my_label = self.recipe_labels[recipe.slug]
				slot_lbl.text = my_label
				
			slot_img.texture = recipe.texture
			slot.mouse_default_cursor_shape = Control.CURSOR_POINTING_HAND
		else:
			slot_lbl.text = ""
			slot_img.texture = null
			slot.mouse_default_cursor_shape = Control.CURSOR_ARROW
		
		i += 1
