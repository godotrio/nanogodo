extends "res://script/gui/WindowBasedContainer.gd"

const InteractiveInventory = preload("res://script/gui/InteractiveInventoryGui.gd")

var god
var character
var inventory
var ii_ui  # Interactive Inventory GUI

func _init(god, character):
	self.god = god
	self.character = character
	self.inventory = character.toolbelt
	
	self.mfv_anchor_left   = 0.5
	self.mfv_anchor_top    = 1.0
	self.mfv_anchor_right  = 0.5
	self.mfv_anchor_bottom = 1.0

	self.ii_ui = InteractiveInventory.new(god, self.character, self.inventory)
	add_child(self.ii_ui)
#	var iui_size = iui.rect_min_size  # we could also use this
	
	rect_min_size = self.ii_ui.rect_min_size

func update_display():
	self.ii_ui.update_display()
