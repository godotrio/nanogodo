extends Control

const InteractiveInventorySlot = preload("InteractiveInventorySlot.gd")

const SLOTS_LINES_CONTAINER_NODE_NAME = "SlotsLinesContainer"
const SLOTS_LINE_NODE_NAME = "SlotsLine%d"
const SLOT_NODE_NAME = "Slot%d"
const SLOT_LABEL_NODE_NAME = "SlotLabel"
const SLOT_ICON_NODE_NAME = "SlotIcon"

const SLOT_HAND_SPRITE_HFRAMES = 6
const SLOT_HAND_SPRITE_TEXTURE = preload("res://data/base/graphics/ui/hand_slot.png")

var god
var character
var inventory
var hand
var slots_size_x
var slots_size_y


func _init(god, character, inventory=null):
	self.god = god
	self.character = character  # probably always god.game.character
	if null == inventory:
		inventory = character.toolbelt # character.backpack ?
	self.inventory = inventory
	self.hand = character.hand
	
	self.hand.connect("visibility_changed", self, "update_self_display")
	
	self.slots_size_x = self.inventory.slots_size_x
	self.slots_size_y = self.inventory.slots_size_y
	
	create_tree_children()


# Process is used to animate the hand. Should be pretty cheap.
var current_milliframe = 0
var animation_speed = 333 # of the slot hand, in milliframes per process
func _process(delta):
	var slot = self.character.hand.held_inventory_slot
	if slot and self.character.hand.held_inventory == inventory:
		var hand_sprite = slots[slot].get_node(SLOT_ICON_NODE_NAME)
		current_milliframe += animation_speed
		current_milliframe %= 1000 * SLOT_HAND_SPRITE_HFRAMES
		hand_sprite.frame = floor(current_milliframe / 1000.0)

var slots = {} # Vector2 => InteractiveInventorySlot node

func create_tree_children(with_spacing=false):
	assert(get_child_count() == 0)
	
	var slot_rect_side = 30 # px
	var separator_x = 49
	var slot_x_padding = 4
	var slot_y_padding = 4
	var x_padding = 5
	var y_padding = 5
	
#	mfv_anchor_left   = 0.5
#	mfv_anchor_top    = 1.0
#	mfv_anchor_right  = 0.5
#	mfv_anchor_bottom = 1.0
	
	var background = NinePatchRect.new()
	background.name = "JE_SUIS_CHOCAPIC"
	background.texture = preload("res://data/base/graphics/ui/panel.png")
	background.axis_stretch_horizontal = NinePatchRect.AXIS_STRETCH_MODE_TILE_FIT
	background.axis_stretch_vertical = NinePatchRect.AXIS_STRETCH_MODE_TILE_FIT
	background.patch_margin_left   = 5
	background.patch_margin_top    = 5
	background.patch_margin_right  = 5
	background.patch_margin_bottom = 5
	background.anchor_left   = 0
	background.anchor_top    = 0
	background.anchor_right  = 1
	background.anchor_bottom = 1
	
	add_child(background)
	
	var lines_container = VBoxContainer.new()
	lines_container.name = SLOTS_LINES_CONTAINER_NODE_NAME
	lines_container.anchor_left   = 0
	lines_container.anchor_top    = 0
	lines_container.anchor_right  = 1
	lines_container.anchor_bottom = 1
	lines_container.rect_position = Vector2(5, 5) # leave room for background
	
	for y in range(1, self.slots_size_y + 1):
		
		var line = HBoxContainer.new()
		line.name = SLOTS_LINE_NODE_NAME % y
		
		for x in range(1, self.slots_size_x + 1):
			var slot_key = Vector2(x, y)
			var slot = InteractiveInventorySlot.new(self.inventory, slot_key)
			slot.name = SLOT_NODE_NAME % x
			slot.theme = preload("res://data/base/graphics/ui/toolbelt_slot_style.tres")
#			slot.rect_position = Vector2(-1,-1)
			slot.rect_min_size = Vector2(slot_rect_side, slot_rect_side)
			slot.rect_clip_content = true
			slot.connect("slot_pressed", self, "on_slot_pressed")
			
			# We use a Sprite instead, to be able to animate it
#			var slot_img = TextureRect.new()
#			slot_img.name = SLOT_ICON_NODE_NAME
#			slot_img.stretch_mode = TextureRect.STRETCH_KEEP_ASPECT_CENTERED
#			slot_img.rect_position = Vector2(-1,-1)
#			slot_img.mouse_filter = Control.MOUSE_FILTER_IGNORE
#			slot.add_child(slot_img)
			
			# Holds the pickup texture or the hand (animated) texture or nothing
			var slot_img = Sprite.new()
			slot_img.name = SLOT_ICON_NODE_NAME
			slot_img.offset = Vector2(-1, -1)
			slot_img.centered = false
			slot.add_child(slot_img)
			
			var slot_lbl = Label.new()
			slot_lbl.name = SLOT_LABEL_NODE_NAME
			slot_lbl.grow_vertical = Control.GROW_DIRECTION_BEGIN
			slot_lbl.grow_horizontal = Control.GROW_DIRECTION_BEGIN
			slot_lbl.align = Label.ALIGN_RIGHT
			slot_lbl.anchor_top = 0.97
			slot_lbl.anchor_left = 0.96
			slot.add_child(slot_lbl)
			
			line.add_child(slot)
			slots[slot_key] = slot
			
			if with_spacing and x == floor(self.slots_size_x / 2.0):
				#var space = VSeparator.new()
				var space = Control.new()
				space.rect_min_size = Vector2(separator_x, slot_rect_side)
				line.add_child(space)
		
		lines_container.add_child(line)
	
	add_child(lines_container)
	var x_space = 0
	if with_spacing:
		x_space = x_padding + separator_x
	rect_min_size = Vector2(
		slots_size_x * (slot_rect_side + slot_x_padding) + x_padding + x_space,
		slots_size_y * (slot_rect_side + slot_y_padding) + y_padding
	)
	
#	resize()


## ACTIONS #####################################################################

func on_slot_pressed(slot_node, slot_inventory, slot_key, is_some, is_all):
	"""
	Called by the slots nodes when they're pressed.
	"""
	if slot_node.is_empty():
		if hand.is_empty():
			# Slot is empty, and the hand too.
			pass # nothing is cool
		else:
			# Slot is empty, move pickup from hand to inventory
			var hand_held_inventory = hand.held_inventory
			var hand_held_slot = hand.held_inventory_slot
			
			if self.inventory.can_store_type(hand.get_held_pickup().pickup_type):
				if not self.inventory.is_stackable():
					# Take one pickup from the hand
					assert(not "implemented")
				else:
					hand.drop()
					hand_held_inventory.move_to_inventory(
						hand_held_slot,
						self.inventory, slot_key
					)
					update_display()
			else: 
				god.snack("It doesn't fit in here")
	else:
		if hand.is_empty():
			if is_some:
				var toolbelt = god.game.character.toolbelt
				# Slot is occupied, hand is empty
				# We want quick transfers (with SHIFT)
				if self.inventory == toolbelt:
					# On the toolbelt: transfer to any open fixture interface
					var fixture = god.game.character.fixture_with_open_gui
					if fixture:
						var pickup = slot_node.get_pickup()
						if fixture.can_feed_pickup(pickup, null):
							pickup = slot_node.retrieve_pickup()
							fixture.feed_pickup(pickup, null)
				else:
					# Anywhere else: transfer to the toolbelt
					var pickup = slot_node.get_pickup()
					if toolbelt.has_room_for_pickup(pickup):
						pickup = slot_node.retrieve_pickup()
						toolbelt.store(pickup)
			else:
				# The hand is free to grab what's in the slot
				hand.grab(self.inventory, slot_key)
				hand.update_position()
				update_display() # show the "in hand" icon on the grabbed slot
		else:
			var hand_pickup = hand.get_held_pickup()
			var slot_pickup = slot_node.get_pickup()

			if hand_pickup == slot_pickup:
				# That's one way to free the hand… [poker face]
				hand.drop()
				update_display()
			elif hand_pickup.is_of_same_type_as(slot_pickup):
				# Fusion pickups from the hand to the slot
				var max_stack_size = god.pickups[slot_pickup.pickup_type].stack
				var available = max_stack_size - slot_pickup.stack

				var remainder = max(0, hand_pickup.stack - available)
				if remainder > 0:
					hand_pickup.stack -= available
					slot_pickup.stack += available
#					god.spawn_pickup(hand_pickup.pickup_type, available)
				elif remainder == 0:
					slot_pickup.stack += hand_pickup.stack
					hand.held_inventory.remove(hand_pickup, hand.held_inventory_slot)
					hand.drop()
					god.despawn_pickup(hand_pickup)
				else:
					assert(not "okay") # what happened ?!
				update_display()
			else:
				# Exchange pickups, and free the hand
				var hand_held_inventory = hand.held_inventory
				var hand_inventory_slot = hand.held_inventory_slot
				if self.inventory.can_store_type(hand.get_held_pickup().pickup_type):
					hand.drop()
					self.inventory.exchange_with_inventory(
						slot_key,
						hand_held_inventory, hand_inventory_slot
					)
				else: 
					god.snack("It doesn't fit in here")
#				inventory.remove(slot_pickup, slot_key)
#				var internal_exchange = (hand_held_inventory == self.inventory)
#				hand.drop()
#				hand_held_inventory.move_to_inventory(
##				move_pickups_between_inventories(
##					hand_held_inventory,
#					hand_inventory_slot,
#					self.inventory, slot_key
#				)
##				hand.drop()
##				hand.grab(hand_held_inventory, hand_inventory_slot)
#				hand_held_inventory.store(
#					slot_pickup, hand_inventory_slot, not internal_exchange
#				)
#				if internal_exchange:
#					self.inventory.
#				hand.drop()

#				update_display()


### DISPLAY ####################################################################

func update_self_display():
	self.character.hand.update_display()	
	var regular_modulate = Color(1,1,1,1)
	var ghost_modulate = Color(1,1,1,0.618*0.618)
	for slot_key in slots:
		var slot = slots[slot_key]
		assert(slot.has_node(SLOT_ICON_NODE_NAME))
		var slot_img = slot.get_node(SLOT_ICON_NODE_NAME)
		assert(slot.has_node(SLOT_LABEL_NODE_NAME))
		var slot_lbl = slot.get_node(SLOT_LABEL_NODE_NAME)
		
		var is_slot_of_hand = \
			self.character.hand.held_inventory == inventory and \
			self.character.hand.held_inventory_slot == slot_key
		var pickup = inventory.find_at(slot_key)
		
		if is_slot_of_hand:
			slot_lbl.text = ""
			slot_img.hframes = SLOT_HAND_SPRITE_HFRAMES
			slot_img.texture = SLOT_HAND_SPRITE_TEXTURE
			slot_img.modulate = regular_modulate
		elif pickup and is_instance_valid(pickup):
			slot_lbl.text = str(pickup.stack)
			slot_img.frame = 0
			slot_img.hframes = 1
			slot_img.texture = pickup.texture
			slot_img.modulate = regular_modulate
	#			slot_img.update()
			slot.mouse_default_cursor_shape = Control.CURSOR_POINTING_HAND					
		elif inventory.has_pickup_type_constraint():
			var pickup_type = inventory.get_pickup_type_constraint()
			slot_lbl.text = ""
			slot_img.frame = 0
			slot_img.hframes = 1
			slot_img.texture = god.pickups[pickup_type].texture
			slot_img.modulate = ghost_modulate
			slot.mouse_default_cursor_shape = Control.CURSOR_ARROW
		else:
			slot_lbl.text = ""
			slot_img.frame = 0
			slot_img.hframes = 1
			slot_img.texture = null
			slot_img.modulate = regular_modulate
			slot.mouse_default_cursor_shape = Control.CURSOR_ARROW

func update_display():
	update_self_display()
