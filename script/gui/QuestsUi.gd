extends CenterContainer

var god
var game
var level

func _init(god):
	self.god = god
	self.game = god.game
	self.level = god.game.level
	
	self.theme = preload("res://data/base/graphics/ui/hud_theme.tres")
	
	self.anchor_top = 0
	self.anchor_left = 0
	self.anchor_right = 1
	self.anchor_bottom = 1
	self.margin_top = -100
	
#	self.rect_min_size = Vector2(500,200)
	
	var vbox = VBoxContainer.new()
	
#	var background = NinePatchRect.new()
	
	var background = Panel.new()
	background.anchor_top = 0
	background.anchor_left = 0
	background.anchor_right = 1
	background.anchor_bottom = 1
	background.margin_top = -15
	background.margin_right = 16
	background.margin_bottom = 184
	background.margin_left = -16
	background.show_behind_parent = true
	
	var font = DynamicFont.new()
	font.font_data = preload("res://data/base/fonts/hexagon_cup_bold.ttf")
	font.size = 31
	
	var label = Label.new()
	label.text = "MISSION  PARAMETERS"
	label.add_font_override('font', font)
	label.add_color_override('font_color_shadow', Color(0,0,0))
	
	var msg = Label.new()
	msg.text = self.level.get_mission_statement()
	
	var quit_button = Button.new()
	quit_button.text = "ACKNOWLEDGED"
	quit_button.margin_top = 300
	quit_button.rect_min_size = Vector2(0, 50)
	quit_button.connect("pressed", self, "on_quit", [])
	
	label.add_child(background)
	vbox.add_child(label)
	vbox.add_child(msg)
	vbox.add_child(quit_button)
	add_child(vbox)


################################################################################

func on_quit():
	close()

func close():
	if get_parent():
		get_parent().remove_child(self)
	self.queue_free()
