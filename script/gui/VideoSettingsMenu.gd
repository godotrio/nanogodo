extends Control

const buttons_name = [ "RESOLUTION", "VSYNC", "FULLSCREEN", "BORDERLESS", "RESIZABLE", "APPLY" ,"CANCEL" ]

onready var fullscreen_button = get_node("BackgroundContainer/MarginContainer/VBoxContainer/MarginContainer/ScrollContainer/MarginContainer/VBoxContainer/FullscreenButton")
onready var vsync_button = get_node("BackgroundContainer/MarginContainer/VBoxContainer/MarginContainer/ScrollContainer/MarginContainer/VBoxContainer/VsyncButton")
onready var borderless_button = get_node("BackgroundContainer/MarginContainer/VBoxContainer/MarginContainer/ScrollContainer/MarginContainer/VBoxContainer/BorderlessButton")
onready var resolution_button = get_node("BackgroundContainer/MarginContainer/VBoxContainer/MarginContainer/ScrollContainer/MarginContainer/VBoxContainer/ResolutionButton")
onready var resizable_button = get_node("BackgroundContainer/MarginContainer/VBoxContainer/MarginContainer/ScrollContainer/MarginContainer/VBoxContainer/ResizableButton")
onready var apply_button = get_node("BackgroundContainer/MarginContainer/VBoxContainer/MarginContainer/ScrollContainer/MarginContainer/VBoxContainer/ApplyButton")
onready var cancel_button = get_node("BackgroundContainer/MarginContainer/VBoxContainer/MarginContainer/ScrollContainer/MarginContainer/VBoxContainer/CancelButton")

var __resolution
var __vsync
var __fullscreen
var __borderless
var __resizable

onready var app_node = get_node("/root/App")

func _ready():
	__resolution = OS.window_size

	__vsync = app_node.config_dict.Video.vsync
	__fullscreen = app_node.config_dict.Video.fullscreen
#	__borderless = app_node.config_dict.Video.borderless
	__resizable = app_node.config_dict.Video.resizable
	vsync_button.pressed = __vsync
	fullscreen_button.pressed = __fullscreen
#	borderless_button.pressed = __borderless
	resizable_button.pressed = __resizable

	set_resolution_button(!fullscreen_button.pressed)
	set_resizable_button(!fullscreen_button.pressed)
	set_borderless_button(false)
	
	resolution_button.connect("pressed", self, "resolution_action")
	vsync_button.connect("pressed", self, "vsync_action")
	fullscreen_button.connect("pressed", self, "fullscreen_action")
	borderless_button.connect("pressed", self, "borderless_action")
	resizable_button.connect("pressed", self, "resizable_action")
	apply_button.connect("pressed", self, "apply")
	cancel_button.connect("pressed",self, "cancel")
	
	resolution_button.set_text(buttons_name[0])
	vsync_button.set_text(buttons_name[1])
	fullscreen_button.set_text(buttons_name[2])
	borderless_button.set_text(buttons_name[3])
	resizable_button.set_text(buttons_name[4])
	apply_button.set_text(buttons_name[5])
	cancel_button.set_text(buttons_name[6])
	pass

var menu_node
func set_menu_node(menu_node):
	self.menu_node = menu_node


func resolution_action():
	menu_node.load_resolution_settings_menu()
	pass

func vsync_action():
	__vsync = vsync_button.pressed
	pass

func fullscreen_action():
	__fullscreen = fullscreen_button.pressed
	set_resolution_button(!fullscreen_button.pressed)
	set_resizable_button(!fullscreen_button.pressed)
	set_borderless_button(false)
	pass

func borderless_action():
	#__borderless = borderless_button.pressed
	#OS.alert("Wait for Godot 3.1 to fix MouseMode", "Not right now")
	pass

func resizable_action():
	__resizable = resizable_button.pressed
	pass

func apply():
	if menu_node.name == "Menu":
		menu_node.set_screen_settings_and_confirm(__resolution, __fullscreen, false, __resizable, __vsync)
	else:
		OS.alert( "Please load a parent that can apply settings (Menu.tscn)", "Parent scene doesn't hold this method" )


func cancel():
	menu_node.load_settings_menu()
	pass



func set_borderless_button(value):
	borderless_button.disabled = !value
	pass
func set_resizable_button(value):
	resizable_button.disabled = !value
	pass
func set_resolution_button(value):
	resolution_button.disabled = !value
	pass
