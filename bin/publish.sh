#!/usr/bin/env bash

VERSION=`bash bin/get_version.sh`
HTML5="nanogodo_${VERSION}_html5"

REMOTE="ljbac.com"
REMOTE_DIR="/var/www/com.goutenoir.antoine/public/games"

## RSYNC HTML5 WITH SERVER
rsync -vr \
	build/html5/${HTML5}/* \
	${REMOTE}:${REMOTE_DIR}/nanogodo/online

## RSYNC BINARIES WITH SERVER
rsync -vr \
    --exclude="build/html5" \
    build/ \
    ${REMOTE}:${REMOTE_DIR}/nanogodo/downloads
