shader_type canvas_item;

//render_mode unshaded;
//render_mode blend_add;
//render_mode blend_mul;
//render_mode blend_sub;

//uniform float brightness = 1.0;
//uniform float contrast = 1.0;
//uniform float saturation = 1.0;

//uniform float size_x=0.008;
//uniform float size_y=0.008;

uniform sampler2D tile_tex;

varying vec2 world_pos;
varying vec2 my_uv;

void vertex () {
//	world_pos = (WORLD_MATRIX * vec4(VERTEX, 1.0)).xyz;
//	world_pos = (PROJECTION_MATRIX * (EXTRA_MATRIX * (WORLD_MATRIX * vec4(VERTEX, 0.0, 1.0)))).xy;
//	world_pos = (WORLD_MATRIX * ((vec4(VERTEX, 1.0, 1.0)))).xy;
	world_pos = VERTEX;
	my_uv = UV;
}

void fragment() {
	
	vec2 tile = vec2(-3.0,1.0);

//	vec3 a = vec3(1,0,0);
	vec3 a = COLOR.rgb;
//	sampler2D t = SCREEN_TEXTURE;
//	vec3 c = textureLod(SCREEN_TEXTURE, SCREEN_UV+vec2(5,5), 0.0).rgb;

//	vec4 tx = texture(tile_tex, 0.008*FRAGCOORD.xy);
	float t = TIME * 0.1;
	vec4 tx = texture(tile_tex, vec2(abs(sin(t)), abs(cos(t))));
//	COLOR.rgb = tx.rgb;

//	vec4 tx = texture(tile_tex, VERTEX*5.0);
//	COLOR.r = min(1.0, world_pos.x);
//	if (world_pos.x > 0.0) {
//	}
//	COLOR.g = my_uv.y;
	
	
	
	// RAINBOOOOOOW
//	COLOR.r = abs(sin(TIME*1.0));
//	COLOR.g = abs(sin(TIME*1.0 + 3.14 * 2.0 / 3.0));
//	COLOR.b = abs(sin(TIME*1.0 + 3.14 * 4.0 / 3.0));
	
	
//	COLOR.a = 1.0;
//
//	COLOR.rgb = vec3(0.333,0.333,0.333);
//	COLOR.r = abs(world_pos.y);
	
	

//	vec3 c = textureLod(SCREEN_TEXTURE,SCREEN_UV,0.0).rgb;

//	c.rgb = mix(vec3(0.0), c.rgb, brightness);
	//c.rgb = mix(vec3(0.5), c.rgb, contrast);
	//c.rgb = mix(vec3(dot(vec3(1.0), c.rgb)*0.33333), c.rgb, saturation);
//	if (SCREEN_UV.x < 0.42 && SCREEN_UV.y < 0.42) {
//		COLOR.rgb = a;
//		COLOR.a = 1.0;
//	}

// vec4 FRAGCOORD : Fragment coordinates, pixel adjusted.
// - x (0 on the left, looking right)
// - y (0 at the bottom, looking up)

//	COLOR.rgb = COLOR.brg;
//	COLOR.r = COLOR.r + 0.8;
	
//	COLOR.r = FRAGCOORD.x / SCREEN_PIXEL_SIZE.x;
//	COLOR.b = FRAGCOORD.y / SCREEN_PIXEL_SIZE.y;
//	if (FRAGCOORD.z > 3.0) {
//		COLOR.rgb = vec3(1.0,0,0);
//	} else {
//		COLOR.rgba = FRAGCOORD.rgba;
//	}
//	if (VERTEX.x < 0.32 && VERTEX.y < 0.32) {
//		COLOR.rgb = vec3(0,1,0);
//		COLOR.a = 0.5;
//	}
	
	
	
	
	
//	vec2 uv = SCREEN_UV;
//	uv -= mod(uv,vec2(size_x,size_y));
	
//	uv += vec2(5.0,5.0);
	
//	COLOR.rgb = textureLod(SCREEN_TEXTURE,uv,0.0).rgb;
	
	// Not even defined. Sigh.
	//ALBEDO = a;

}
