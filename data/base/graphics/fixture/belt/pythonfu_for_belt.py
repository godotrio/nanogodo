"""
[ < gimp.Layer
'==TEMPLATE==' >
< gimp.Layer
'template sideway' >
< gimp.Layer
'template in game' >
< gimp.Layer
'===STRIPE===' >
< gimp.Layer
'*******iMAIN******' >
< gimp.Layer
'white stripe right' >
< gimp.Layer
'intake upleft not left not downleft end' >
< gimp.Layer
'intake upleft not left not downleft begin' >
< gimp.Layer
'intake upleft not left downleft end' >
< gimp.Layer
'intake upleft not left downleft begin' >
< gimp.Layer
'intake upleft end' >
< gimp.Layer
'intake upleft begin' >
< gimp.Layer
'intake upright right not upleft not left not downleft downright end' >
< gimp.Layer
'intake upright right not upleft not left not downleft downright begin' >
< gimp.Layer
'intake upright not right not upleft not left not downleft downright end' >
< gimp.Layer
'intake upright not right not upleft not left not downleft downright begin' >
< gimp.Layer
'intake upright right not upleft not left not downleft not downright end' >
< gimp.Layer
'intake upright right not upleft not left not downleft not downright begin' >
< gimp.Layer
'intake upright not right not upleft not left not downleft not downright end' >
< gimp.Layer
'intake upright not right not upleft not left not downleft not downright begin' >
< gimp.Layer
'intake upright end' >
< gimp.Layer
'intake upright begin' >
< gimp.Layer
'******iREPLICA******' >
< gimp.Layer
'intake downleft not upleft not left end' >
< gimp.Layer
'intake downleft not upleft not left begin' >
< gimp.Layer
'intake downleft upleft not left end' >
< gimp.Layer
'intake downleft upleft not left begin' >
< gimp.Layer
'intake downleft end' >
< gimp.Layer
'intake downleft begin' >
< gimp.Layer
'intake downright right upright not upleft not left not downleft end' >
< gimp.Layer
'intake downright right upright not upleft not left not downleft begin' >
< gimp.Layer
'intake downright not right upright not upleft not left not downleft end' >
< gimp.Layer
'intake downright not right upright not upleft not left not downleft begin' >
< gimp.Layer
'intake downright right not upright not upleft not left not downleft end' >
< gimp.Layer
'intake downright right not upright not upleft not left not downleft begin' >
< gimp.Layer
'intake downright not right not upright not upleft not left not downleft end' >
< gimp.Layer
'intake downright not right not upright not upleft not left not downleft begin' >
< gimp.Layer
'intake downright end' >
< gimp.Layer
'intake downright begin' >
< gimp.Layer
'====BORDER===' >
< gimp.Layer
'********MAIN******' >
< gimp.Layer
'Border right upright and a LEFT' >
< gimp.Layer
'Border upright upleft' >
< gimp.Layer
'Border upright upleft not left' >
< gimp.Layer
'Border upleft not left not downleft' >
< gimp.Layer
'Border upleft not left' >
< gimp.Layer
'Border upleft' >
< gimp.Layer
'Border right upright not upleft not left not downleft not downright' >
< gimp.Layer
'Border not right upright not upleft not left not downleft not downright' >
< gimp.Layer
'Border right' >
< gimp.Layer
'Border upright not upleft not left not downleft' >
< gimp.Layer
'Border upright' >
< gimp.Layer
'******REPLICA******' >
< gimp.Layer
'Border right downright and a LEFT' >
< gimp.Layer
'Border downleft downright' >
< gimp.Layer
'Border not left downright downleft' >
< gimp.Layer
'Border not left not upleft downleft' >
< gimp.Layer
'Border not left downleft' >
< gimp.Layer
'Border downleft' >
< gimp.Layer
'Border downright' >
< gimp.Layer
'Border right not upright not upleft not left not downleft downright' >
< gimp.Layer
'Border not right not upright not upleft not left not downleft downright' >
< gimp.Layer
'Border right =useless=' >
< gimp.Layer
'Border not upleft not left not downleft downright' >
< gimp.Layer
'Border' >
< gimp.Layer
'=BACKGROUND=' >
< gimp.Layer
'Background left' >
< gimp.Layer
'Background right' >
< gimp.Layer
'==SIDEWAY==' >
< gimp.Layer
'sideway left stripe' >
< gimp.Layer
'sideway right stripe' >
< gimp.Layer
'sideway left tight' >
< gimp.Layer
'sideway right tght' >
< gimp.Layer
'sideway right straight' >
< gimp.Layer
'sideway left straight' >]
"""

def get_image():
    for i in gimp.image_list():
        if i.name == 'next_general.xcf':
            image = i
        return image


def reset_layers(image):
    for i in image.layers:
        i.visible = False


def enable_layer_list(image, list):
    for i in image.layers:
        for j in list:
            if i.name == j:
                i.visible = True


def enable_layer_list_combo(image, index):
    for i in combo[index]:
        enable_layer_list(image, i)


#	for i in image.layers:
#		if i.name == 'Background right' or i.name == 'Background left':
#			i.visible = True

def create_array_possibility():
    for i in range(64):
        print(i)
        combo.append([])
        combo_id.append([])
        combo_name.append([])
        if i & 1 == 1:
            combo[i].append(right)
            combo_name[i].append('(1,0)')
            combo_id[i].append('right')
        if i & 2 == 2:
            combo[i].append(downright)
            combo_name[i].append('(0,1)')
            combo_id[i].append('downright')
        if i & 4 == 4:
            combo[i].append(downleft)
            combo_name[i].append('(-1,1)')
            combo_id[i].append('downleft')
        if i & 8 == 8:
            combo[i].append(left)
            combo_name[i].append('(-1,0)')
            combo_id[i].append('left')
        if i & 16 == 16:
            combo[i].append(upleft)
            combo_name[i].append('(0,-1)')
            combo_id[i].append('upleft')
        if i & 32 == 32:
            combo[i].append(upright)
            combo_name[i].append('(1,-1)')
            combo_id[i].append('upright')
        # LEFT
        bright = False
        bupright = False
        bleft = False
        bupleft = False
        bdownleft = False
        bdownright = False
        if 'right' in combo_id[i]:
            bright = True
        if 'upright' in combo_id[i]:
            bupright = True
        if 'upleft' in combo_id[i]:
            bupleft = True
        if 'left' in combo_id[i]:
            bleft = True
        if 'downleft' in combo_id[i]:
            bdownleft = True
        if 'downright' in combo_id[i]:
            bdownright = True
        one_left = False
        if bupleft or bleft or bdownleft:
            one_left = True
        if bright and (one_left or (not one_left and not bupright and not bdownright)):
            combo[i].append(right_and_a_LEFT)
        # A
        if bright and bupright and one_left:
            combo[i].append(right_upright_and_a_LEFT)
        if bright and bdownright and one_left:
            combo[i].append(right_downright_and_a_LEFT)
        # B
        if bupleft and bupright:
            combo[i].append(upright_upleft)
        if bdownleft and bdownright:
            combo[i].append(downleft_downright)
        # C
        if bupleft and bupright and not bleft:
            combo[i].append(upright_upleft_not_left)
        if bdownleft and bdownright and not bleft:
            combo[i].append(not_left_downleft_downright)
        # D
        if bupleft and not bleft and not bdownleft:
            combo[i].append(upleft_not_left_not_downleft)
        if not bleft and not bupleft and bdownleft:
            combo[i].append(not_left_not_upleft_downleft)
        # E
        if bupleft and not bleft:
            combo[i].append(upleft_not_left)
        if not bleft and bdownleft:
            combo[i].append(not_left_downleft)
        # E&E
        if bupleft and not bleft and bdownleft:
            combo[i].append(upleft_not_left_downleft)
        # F done in boolean logic
        # G
        if bright and bupright and not one_left and not bdownright:
            combo[i].append(right_upright_not_upleft_not_left_not_downleft_not_downright)
        if bright and not bupright and not one_left and bdownright:
            combo[i].append(right_not_upright_not_upleft_not_left_not_downleft_downright)
            print("pouet")
        # H
        if not bright and bupright and not one_left and not bdownright:
            combo[i].append(not_right_upright_not_upleft_not_left_not_downleft_not_downright)
        if not bright and not bupright and not one_left and bdownright:
            combo[i].append(not_right_not_upright_not_upleft_not_left_not_downleft_downright)
        # I
        if bupright and not one_left:
            combo[i].append(upright_not_upleft_not_left_not_downleft)
        if not one_left and bdownright:
            combo[i].append(not_upleft_not_left_not_downleft_downright)
        # I&I
        if bupright and not one_left and bdownright:
            combo[i].append(upright_and_not_a_LEFT_and_downright)
        # J done in boolean logic
        if one_left:
            combo[i].append(background_right)
        if not (one_left or bupright or bdownright):
            combo[i].append(background_right)
            combo[i].append(left)
        #K
        if bupleft and bleft:
            combo[i].append(upleft_and_left)
        if bleft and bdownleft:
            combo[i].append(left_and_downleft)
        #L
        if bupright and not one_left and not bdownright:
            combo[i].append(upright_and_not_a_LEFT_and_not_downright)
        if bdownright and not bupright and not one_left:
            combo[i].append(downright_and_not_upright_and_not_a_LEFT)
        #M
        if bupright and one_left:
            combo[i].append(upright_and_a_LEFT)
        if bdownright and one_left:
            combo[i].append(downright_and_a_LEFT)
        #N
        if bupright and bupleft:
            combo[i].append(upright_and_upleft_fix)
        if bdownleft and bdownright:
            combo[i].append(downleft_and_downright_fix)
        #if bright and one_left:
        if not bright and bupright and not one_left and bdownright:
            combo[i].append(not_right_upright_and_not_a_LEFT_and_downright_fix)
def remove_not_visible_layers(image):
    for i in image.layers:
        if i.visible == False:
            image.remove_layer(i)


# A
right_upright_and_a_LEFT = []
right_upright_and_a_LEFT.append('Border right upright and a LEFT')
# B
upright_upleft = []
upright_upleft.append('Border upright upleft')
# C
upright_upleft_not_left = []
upright_upleft_not_left.append('Border upright upleft not left')
# D
upleft_not_left_not_downleft = []
upleft_not_left_not_downleft.append('Border upleft not left not downleft')
upleft_not_left_not_downleft.append('intake upleft not left not downleft end')
upleft_not_left_not_downleft.append('intake upleft not left not downleft begin')
# E
upleft_not_left = []
upleft_not_left.append('Border upleft not left')
# E&E
upleft_not_left_downleft = []
upleft_not_left_downleft.append('intake upleft not left downleft end')
upleft_not_left_downleft.append('intake upleft not left downleft begin')
upleft_not_left_downleft.append('intake downleft upleft not left end')
upleft_not_left_downleft.append('intake downleft upleft not left begin')
# F
upleft = []
upleft.append('Border upleft')
# G
right_upright_not_upleft_not_left_not_downleft_not_downright = [
    'Border right upright not upleft not left not downleft not downright',
    'FIX right upright not upleft not left not downleft not downright'
]
# H
not_right_upright_not_upleft_not_left_not_downleft_not_downright = []
not_right_upright_not_upleft_not_left_not_downleft_not_downright.append(
    'Border not right upright not upleft not left not downleft not downright')

right = []
right.append('Border right')

right_and_a_LEFT = []
right_and_a_LEFT.append('white stripe right')
#I
upright_not_upleft_not_left_not_downleft = []
upright_not_upleft_not_left_not_downleft.append('Border upright not upleft not left not downleft')
# I&I
upright_and_not_a_LEFT_and_downright = [
    'intake upright not upleft not left not downleft downright end',
    'intake upright not upleft not left not downleft downright begin',
    'intake downright upright not upleft not left not downleft end',
    'intake downright upright not upleft not left not downleft begin',
]
right_and_upright_and_not_a_LEFT_and_downright = [
    'FIX right upright not upleft not left not downleft downright',
]
# J
upright = []
upright.append('Border upright')
#K
upleft_and_left = [
    'intake upleft end',
    'intake upleft begin'
]
#L
upright_and_not_a_LEFT_and_not_downright = [
    'intake upright not upleft not left not downleft not downright end',
    'intake upright not upleft not left not downleft not downright begin',
]
#M
upright_and_a_LEFT = [
    'intake upright end',
    'intake upright begin'
]
#N
upright_and_upleft_fix = [
    'upright upleft fix'
]
not_right_upright_and_not_a_LEFT_and_downright_fix = [
    'Border upright and downright Fix'
]

#A
right_downright_and_a_LEFT = []
right_downright_and_a_LEFT.append('Border right downright and a LEFT')
#B
downleft_downright = []
downleft_downright.append('Border downleft downright')
#C
not_left_downleft_downright = []
not_left_downleft_downright.append('Border not left downleft downright')
#D
not_left_not_upleft_downleft = []
not_left_not_upleft_downleft.append('Border not left not upleft downleft')
not_left_not_upleft_downleft.append('intake downleft not upleft not left end')
not_left_not_upleft_downleft.append('intake downleft not upleft not left begin')
#E
not_left_downleft = []
not_left_downleft.append('Border not left downleft')
#F
downleft = []
downleft.append('Border downleft')
#G
right_not_upright_not_upleft_not_left_not_downleft_downright = [
    'FIX right not upright not upleft not left not downleft downright',
    'Border right not upright not upleft not left not downleft downright'
]
#H
not_right_not_upright_not_upleft_not_left_not_downleft_downright = []
not_right_not_upright_not_upleft_not_left_not_downleft_downright.append(
    'Border not right not upright not upleft not left not downleft downright')
#I
not_upleft_not_left_not_downleft_downright = []
not_upleft_not_left_not_downleft_downright.append('Border not upleft not left not downleft downright')
#J
downright = []
downright.append('Border downright')

#K
left_and_downleft =  [
    'intake downleft end',
    'intake downleft begin'
]
#L
downright_and_not_upright_and_not_a_LEFT = [
    'intake downright not upright not upleft not left not downleft end',
    'intake downright not upright not upleft not left not downleft begin'
]
#M
downright_and_a_LEFT = [
    'intake downright end',
    'intake downright begin'
]
#N
downleft_and_downright_fix = [
    'downleft downright fix'
]
left = []
left.append('Background left')

background_right = []
background_right.append('Background right')

combo = []
combo_id = []
combo_name = []
right = []
right.append('Border right')
#right.append('white stripe right')
down_right = []
down_right.append('Border downright not right')
# down_right.append('Border downright')
down_right.append('intake downright end')
down_right.append('intake downright begin')
down_left = []
# down_left.append('Border downleft')
down_left.append('intake downleft end')
down_left.append('intake downleft begin')
up_right = []
up_right.append('Border upright not right')
# up_right.append('Border upright')
up_right.append('intake upright end')
up_right.append('intake upright begin')
up_left = []
# up_left.append('Border upleft')
up_left.append('intake upleft end')
up_left.append('intake upleft begin')


def main_generator():
    create_array_possibility()
    filename = "F:\gimp_work\\next_general.xcf"
    for i in range(64):
        file_name = "belt_"
        for j in combo_name[i]:
            file_name += j
        file_name += ".png"
        file_path = "F:\gimp_work\\" + file_name
        image = pdb.gimp_file_load(filename, filename)
        display = gimp.Display(image)
        # reset_layers(image)
        # time.sleep(5)
        # time.sleep(5)
        enable_layer_list_combo(image, i)
        # print(combo)
        image.merge_visible_layers(0)
        remove_not_visible_layers(image)
        layer = image.layers[0]
        pdb.gimp_file_save(image, layer, file_path, file_path)
    # pdb.gimp_display_delete(display)


#		gimp.image_list()


image.merge_visible_layers(GIMP_FLATTEN_IMAGE)

buffer_image = gimp.Image(image.width, image.height, image.base_type)